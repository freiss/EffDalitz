/*!
 *  @file      commonLib.cpp
 *  @author    Alessio Piucci
 *  @brief     Common library of methods used by different macros and structures.
 */

#include "commonLib.h"

using namespace std;

//-----------------------------------
// Parse a string containing some environment variables
//-----------------------------------
std::string ParseEnvName(std::string input_string){
  
  //I first search the env variable in the input string
  size_t start_pos = input_string.find("$");
  
  if(start_pos == std::string::npos)
    return input_string;  //if no $ are find, there are no env variables to convert
  else
  {
    std::string env_variable = input_string.substr(start_pos, input_string.find("/", start_pos + 1));
    
    //to convert the env variable, I have to skip the first $ char
    char *env_variable_converted = getenv((env_variable.substr(1, env_variable.size())).c_str());

    std::cout << "Converted " << env_variable << " --> " << env_variable_converted << std::endl;
    
    input_string.replace(start_pos, input_string.find("/", start_pos + 1), env_variable_converted);

    //now search for the next occurance of an env variable
    return ParseEnvName(input_string);
  }
}

//-----------------------------------
// Compute the binomial error, mainly for efficiency computation
//-----------------------------------
double BinomialError(int passed, int tries){
  return sqrt(passed * (tries - passed) / (double) pow(tries, 3.));
}

//-----------------------------------
// Compute the error of a generic ratio
//-----------------------------------
double ErrorRatio(double num, double num_err, double den, double den_err){
  return sqrt( pow(num_err/den, 2.)
               + pow(num*den_err/(pow(den, 2.)), 2.));
}

//-----------------------------------
//  Search for entry errors for a TEfficiency, TH2D
//-----------------------------------
void FixTEfficiencyEntries(TH2D* h_pass, TH2D* h_total, unsigned int &num_fixed_bins){
  
  //check: pass <= total, and positive content
  
  //loop over x bins
  for(int i_xbin = 0; i_xbin <= h_pass->GetNbinsX(); ++i_xbin){
    
    //loop over y bins
    for(int i_ybin = 0; i_ybin <= h_pass->GetNbinsY(); ++i_ybin){
      
      if((h_pass->GetBinContent(i_xbin, i_ybin) > h_total->GetBinContent(i_xbin, i_ybin))){
        std::cout << "Found (i_xbin, i_ybin) = (" << i_xbin << ", " << i_ybin << ")"
                  << ", h_total = " << h_total->GetBinContent(i_xbin, i_ybin) 
                  << ", h_pass = " << h_pass->GetBinContent(i_xbin, i_ybin) << std::endl;
        std::cout << "WARNING: setting total and passed content of bins = 1." << std::endl;
        std::cout << std::endl;
        
        //fix the bin content
        h_total->SetBinContent(i_xbin, i_ybin, 1.);
        h_pass->SetBinContent(i_xbin, i_ybin, 1.);
        
        ++num_fixed_bins;
      }
      if((h_pass->GetBinContent(i_xbin, i_ybin) < 0.) || (h_total->GetBinContent(i_xbin, i_ybin) < 0.)){
        std::cout << "Found (i_xbin, i_ybin) = (" << i_xbin << ", " << i_ybin << ")"
                  << ", h_total = " << h_total->GetBinContent(i_xbin, i_ybin) 
                  << ", h_pass = " << h_pass->GetBinContent(i_xbin, i_ybin) << std::endl;
        std::cout << "WARNING: setting total and passed content of bins = 0." << std::endl;
        std::cout << std::endl;
        
        //fix the bin content
        h_total->SetBinContent(i_xbin, i_ybin, 0.);
        h_pass->SetBinContent(i_xbin, i_ybin, 0.);
        
        ++num_fixed_bins;
      }




    }  //loop over y bins
  }  //loop over x bins
  
  return;
}

//-----------------------------------
//  Search for entry errors for a TEfficiency, TH3D
//-----------------------------------
void FixTEfficiencyEntries(TH3D* h_pass, TH3D* h_total, unsigned int &num_fixed_bins){
  
  //check: pass <= total, and positive content
  
  //loop over x bins
  for(int i_xbin = 0; i_xbin <= h_pass->GetNbinsX(); ++i_xbin){
    
    //loop over y bins
    for(int i_ybin = 0; i_ybin <= h_pass->GetNbinsY(); ++i_ybin){
      
      //loop over z bins
      for(int i_zbin = 0; i_zbin <= h_pass->GetNbinsZ(); ++i_zbin){
        
        if((h_pass->GetBinContent(i_xbin, i_ybin, i_zbin) > h_total->GetBinContent(i_xbin, i_ybin, i_zbin))
           || (h_pass->GetBinContent(i_xbin, i_ybin, i_zbin) < 0.) || (h_total->GetBinContent(i_xbin, i_ybin, i_zbin) < 0.)){
          std::cout << "Found (i_xbin, i_ybin) = (" << i_xbin << ", " << i_ybin << ", " << i_zbin << ")" 
                    << ", h_total = " << h_total->GetBinContent(i_xbin, i_ybin, i_zbin)
                    << ", h_pass = " << h_pass->GetBinContent(i_xbin, i_ybin, i_zbin) << std::endl;
          std::cout << "WARNING: setting total and passed content of bins = 0." << std::endl;
          std::cout << std::endl;
          
          //fix the bin content
          h_total->SetBinContent(i_xbin, i_ybin, i_zbin, 0.);
          h_pass->SetBinContent(i_xbin, i_ybin, i_zbin, 0.);
          
          ++num_fixed_bins;
        }
      }  //loop over z bins
    }  //loop over y bins
  }  //loop over x bins
  
  std::cout << std::endl;
  
  return;
}

//----------------------------------- 
//  Fix weights problems for a TEfficiency, TH2D
//----------------------------------- 
void FixTEfficiencyWeights(TH2D* h_pass, TH2D* h_total){
  
  double stat_pass[TH1::kNstat];
  double stat_total[TH1::kNstat];
  
  h_pass->GetStats(stat_pass);
  h_total->GetStats(stat_total);
  
  //require: sum of weights == sum of weights^2
  if((fabs(stat_pass[0] - stat_pass[1]) > 1e-5) ||
     (fabs(stat_total[0] - stat_total[1]) > 1e-5)) 
    std::cout << "stat_total[0] = " << stat_total[0] 
              << ", stat_total[1] = " << stat_total[1]
              << ", stat_pass[0] = " << stat_pass[0]
              << ", stat_pass[1] = " << stat_pass[1]
              << std::endl;
  
  std::cout << std::endl;
  
  return;
}
