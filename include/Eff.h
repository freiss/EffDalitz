#ifndef INCLUDE_EFF_H
#define INCLUDE_EFF_H 1

/** @class Eff Eff.h include/Eff.h
 *  
 *
 *  @author  Alessio Piucci
 *  @date    12-07-2016
 *  @brief   A class which provides basic tools to compute efficiencies.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser
#include <utility>   //to use std::pair
#include <fstream>   //to write the log in an external stream

#include "commonLib.h"

//ROOT libraries
#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TEntryList.h>
#include <TLorentzVector.h>
#include <TEfficiency.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>

using namespace std;
namespace pt = boost::property_tree;

class Eff {
public: 

  /// Standard constructor
  Eff(const pt::ptree _configtree, std::string _inFileName, TFile* _outFile);

  ~Eff( ); ///< Destructor
  
  //retrieve and parse the normalization value from an external file
  double NormalizationParsing(std::string frac);
  
  //make a 2D Plot
  TH2D* Make2DPlot(TTree *inTree, const bool generated_ForEff, unsigned int &num_events);

  //compute the efficiency on a 2D plot
  void Compute2DEfficiency(TH2D* h_2D_gen, TH2D* h_2D_reco,
                           unsigned int num_reco_events, unsigned int num_gen_events);
  
  //write the output log
  void WriteLog(){log_stream.close(); return;};
  
protected:

private:

  //input file name
  std::string inFileName;
  
  //output file
  TFile *outFile;
  
  //configuration Boost tree
  pt::ptree configtree;

  std::ofstream log_stream;
  
};
#endif // INCLUDE_EFF_H
