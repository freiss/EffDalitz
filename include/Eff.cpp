/*!
 *  @file      Eff.cpp
 *  @author    Alessio Piucci
 *  @brief     A class which provides basic tools to compute efficiencies.
 */

#include "Eff.h"

//------------
// Constructor
//------------
Eff::Eff(const pt::ptree _configtree, std::string _inFileName, TFile* _outFile)
  : configtree(_configtree) {

  inFileName = _inFileName;
  
  configtree = _configtree;

  outFile = _outFile;
  
  //retrieve the path and name of the out log
  std::string log_name = ParseEnvName(configtree.get<std::string>("options.log_file"));
  
  std::cout << "log_file = " << log_name << std::endl;
  
  //open the output stream for the log
  log_stream.open(log_name, ios::out);
  
  //write some informations in the out log
  log_stream << "#logfile created by the Eff library" << std::endl;
  log_stream << "inFileName = " << inFileName << std::endl;
  log_stream << "outFileName = " << outFile->GetName() << std::endl;
  
}

//----------
// Destructor
//-----------
Eff::~Eff(){
  
}

//-----------------------------------------------------------------------------
// Retrieve and parse the normalization value from an external file
//-----------------------------------------------------------------------------
double Eff::NormalizationParsing(std::string frac){
  
  //is the normalization factor to apply?
  if((bool) configtree.get<unsigned int>(("efficiency.Normalize" + frac))){
    
    //------------------------------------//
    //  retrieve the normalization value  //
    //------------------------------------//
    
    //create empty property tree object
    pt::ptree configtree_norm;
    
    //parse the INFO into the property tree.
    pt::read_info(configtree.get<std::string>("efficiency.Normalize" + frac + "_inFile"), configtree_norm);
    
    return configtree_norm.get<double>("GenLevCutsEff.genlevcut.eff");
    
  }  //if normalization required
  
  return 1.;
  
}

//-------------------
// Make a 2D plot
//-------------------
TH2D* Eff::Make2DPlot(TTree *inTree, const bool generated_ForEff, unsigned int &num_events){
  
  TH2D* h_2D = new TH2D("h_2D",
                        (configtree.get<std::string>("options.title")).c_str(),
                        configtree.get<unsigned int>("options.var1_numbins"),   //number of bins
                        configtree.get<double>("options.var1_low"),             //plot range
                        configtree.get<double>("options.var1_high"),
                        configtree.get<unsigned int>("options.var2_numbins"),   //number of bins
                        configtree.get<double>("options.var2_low"),             //plot range
                        configtree.get<double>("options.var2_high"));
  
  TH1D* h_var_1 = new TH1D("h_var_1",
                           (configtree.get<std::string>("options.var1_title")).c_str(),
                           configtree.get<unsigned int>("options.var1_numbins"), //number of bins
                           configtree.get<double>("options.var1_low"),           //plot range
                           configtree.get<double>("options.var1_high"));
  
  TH1D* h_var_2 = new TH1D("h_var_2",
                           (configtree.get<std::string>("options.var2_title")).c_str(),
                           configtree.get<unsigned int>("options.var2_numbins"),  //number of bins
                           configtree.get<double>("options.var2_low"),            //plot range
                           configtree.get<double>("options.var2_high"));
  
  
  //selection of cuts, for reconstructed of generated candidates
  std::string cuts_string = "";
  
  if(!generated_ForEff){
    cuts_string = configtree.get<std::string>("options.cuts");
    
    //If I have to compute the efficiency, I also append the cuts used for the denominator
    if((bool) configtree.get<unsigned int>("efficiency.EffOverMC")){
      
      if((cuts_string != "") && (configtree.get<std::string>("efficiency.cuts") != ""))
        cuts_string += " && ";
    
      cuts_string += configtree.get<std::string>("efficiency.cuts");
    }  //if((bool) configtree.get<unsigned int>("efficiency.EffOverMC"))
    
    //second kinematic cut?
    if(configtree.get_optional<std::string>("options.cuts2")){
      
      //check if I have a not-empty second kinematic cut
      if(configtree.get<std::string>("options.cuts2") != ""){
        
        if(cuts_string != "")
          cuts_string += " && ";
        
        cuts_string += configtree.get<std::string>("options.cuts2");
      } 
    }  //if(configtree.get_optional<std::string>("cuts2"))
    
    log_stream << "options.cuts = " << cuts_string << std::endl;
  }
  else
  {
    cuts_string = configtree.get<std::string>("efficiency.cuts");
    log_stream << "efficiency.cuts = " << cuts_string << std::endl;
  }
    
  //I use a TEntryList to select only candidates which pass the cuts
  inTree->Draw(">>list_events", cuts_string.c_str(), "entrylist");
  
  TEntryList *list_events = (TEntryList*) gDirectory->Get("list_events");
  
  std::cout << "cuts = " << cuts_string << std::endl;
  std::cout << "inTree->GetEntries() = " << inTree->GetEntries()
            << ", list_events->GetN() = " << list_events->GetN() << std::endl;
  
  //variable used for the normalization of histogram
  num_events = list_events->GetN();
  
  //----------------------------------------//
  //  retrieve the variables from the tree  //
  //----------------------------------------//
  
  //values of the variables
  double var_1, var_2;
  
  //names of the variables, for reconstructed or generated case
  std::string var_1_name, var_2_name;
  
  if(!generated_ForEff){
    var_1_name = configtree.get<std::string>("options.var1_name");
    var_2_name = configtree.get<std::string>("options.var2_name");
  }
  else
  {
    var_1_name = configtree.get<std::string>("efficiency.var1_name");
    var_2_name = configtree.get<std::string>("efficiency.var2_name");
  }
  
  //set the addresses of the variables
  inTree->SetBranchAddress(var_1_name.c_str(), &var_1);
  inTree->SetBranchAddress(var_2_name.c_str(), &var_2);
  
  
  //--------------//
  // reweighting  //
  //--------------//
  
  double weight_2Ddistr;
  
  TH2D *h_weights = new TH2D();
  
  double x_var_value;
  double y_var_value;
  
  //reweight reconstructed candidates, based on a 2D histo
  if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight") && (!generated_ForEff)){
    
    //open the input file with the weights
    TFile *weights_file = TFile::Open((configtree.get<std::string>("reweight.2DReweight.file_name")).c_str(), "READ");
  
    //retrieve 2D histo with weights
    h_weights = (TH2D*) weights_file->Get((configtree.get<std::string>("reweight.2DReweight.name")).c_str());
    
    //names of the variables used for the 2D weight histo
    std::string x_var_name = configtree.get<std::string>("reweight.2DReweight.x_var");
    std::string y_var_name = configtree.get<std::string>("reweight.2DReweight.y_var");
    
    //read x, y values from the input tree
    inTree->SetBranchAddress(x_var_name.c_str(), &x_var_value);
    inTree->SetBranchAddress(y_var_name.c_str(), &y_var_value);
    
    outFile->cd();
    h_weights->Write("2DReweight_weights");
  }  //if((bool) configtree.get<unsigned int>("efficiency.2DReweight"))
  
  
  //--------------------//
  //  loop over events  //
  //--------------------//
                               
  //loop over all events, and fill plot with the related weights
  for(unsigned int i_cand = 0; i_cand < num_events; ++i_cand){
    
    inTree->GetEntry(list_events->GetEntry(i_cand));
    
    /////
    
    //should I reweight?
    if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight") && (!generated_ForEff))
      weight_2Ddistr = h_weights->Interpolate(x_var_value, y_var_value);
    else
      weight_2Ddistr = 1.;
    
    /////
    
    //fill 1-D projections
    h_var_1->Fill(var_1);
    h_var_2->Fill(var_2);
    
    //finally fill the 2D plot
    h_2D->Fill(var_1, var_2, weight_2Ddistr);
    
  }  //loop over mothers candidates
  
  //set some style options for the 2D plot
  h_2D->GetXaxis()->SetTitle((configtree.get<std::string>("options.x_axis_label")).c_str());
  h_2D->GetYaxis()->SetTitle((configtree.get<std::string>("options.y_axis_label")).c_str());
  h_2D->GetXaxis()->SetTitleOffset(1.2);
  h_2D->GetYaxis()->SetTitleOffset(1.3);
  
  h_var_1->GetXaxis()->SetTitle((configtree.get<std::string>("options.x_axis_label")).c_str());
  h_var_1->GetYaxis()->SetTitle("entries");
  h_var_1->GetXaxis()->SetTitleOffset(1.2);
  h_var_1->GetYaxis()->SetTitleOffset(1.3);
  
  h_var_2->GetYaxis()->SetTitle((configtree.get<std::string>("options.y_axis_label")).c_str());
  h_var_2->GetYaxis()->SetTitle("entries");
  h_var_2->GetXaxis()->SetTitleOffset(1.2);
  h_var_2->GetYaxis()->SetTitleOffset(1.3);
  
  outFile->cd();
  
  //write the plots the output file
  if(!generated_ForEff)
    h_2D->Write((configtree.get<std::string>("options.out_name")).c_str());
  else
    h_2D->Write((configtree.get<std::string>("options.out_name") + "_gen").c_str());
  
  h_var_1->Write(("h1_" + configtree.get<std::string>("options.var1_title")).c_str());
  h_var_2->Write(("h1_" + configtree.get<std::string>("options.var2_title")).c_str());
  
  //memory cleaning
  delete h_var_1;
  delete h_var_2;
  
  return h_2D;
}

//----------------------------------------
// Compute the efficiency on a 2D plot
//----------------------------------------
void Eff::Compute2DEfficiency(TH2D* h_2D_gen, TH2D* h_2D_reco,
                              unsigned int num_reco_events, unsigned int num_gen_events){
                                              
  //--------------------------------------------------------//
  //  do we need of renormalize numerator and denominator?  //
  //--------------------------------------------------------//

  //scaling factor, to correctly compute the efficiency for the out log
  double scaling = 1.;
  
  if((bool) configtree.get<unsigned int>(("efficiency.NormalizeNum"))
     || (bool) configtree.get<unsigned int>(("efficiency.NormalizeDen"))){
    
    double NumNorm = NormalizationParsing("Num");
    double DenNorm = NormalizationParsing("Den");
    
    //for simplicity, I only normalize the numerator
    double requiredNumEvents = ((double) num_gen_events)*(NumNorm/DenNorm);
    
    scaling = requiredNumEvents/ (double) num_reco_events;
    
    std::cout << "NORMALIZATION: NumNorm = " << NumNorm << ", DenNorm = " << DenNorm
              << ", requiredNumEvents = " << requiredNumEvents  << std::endl; 
    std::cout << "num_gen_events = " << num_gen_events << std::endl;
    std::cout << "num_reco_events = " << num_reco_events << std::endl;
    std::cout << "scaling = " << scaling << std::endl;
    
    h_2D_reco->Scale(scaling);
    
  }  //if normalization required
  
  //the TEfficiency doesn't like not empty underflow and overflow bins
  h_2D_gen->ClearUnderflowAndOverflow();
  h_2D_reco->ClearUnderflowAndOverflow();
  
  //number of bins which were fixed, to use the TEfficiency
  unsigned int num_fixed_bins = 0;
  
  //check consistency of generated and reco histograms
  if(!TEfficiency::CheckConsistency(*h_2D_reco, *h_2D_gen)){
    std::cout << "Error: reconstructed and generated histos are not consistent!" << std::endl;
    
    FixTEfficiencyEntries(h_2D_reco, h_2D_gen, num_fixed_bins);
    
    //exit(EXIT_FAILURE);
  }
  
  log_stream << "num_fixed_bins = " << num_fixed_bins 
             << " (" << (num_fixed_bins/(double) h_2D_reco->GetNcells())*100 << ")" << std::endl;
  
  TEfficiency *h_2D_eff = new TEfficiency(*h_2D_reco, *h_2D_gen);
  
  //use Wilson statistic, please don't ask me but use this reference:
  //https://root.cern.ch/doc/master/classTEfficiency.html
  h_2D_eff->SetConfidenceLevel(0.95);
  h_2D_eff->SetStatisticOption(TEfficiency::kFWilson);
  
  //set titles
  std::string title = configtree.get<std::string>("efficiency.title");
  std::string x_title = configtree.get<string>("options.x_axis_label");
  std::string y_title = configtree.get<string>("options.y_axis_label");
  std::string title_complete = title + ";" + x_title + ";" + y_title;
  
  h_2D_eff->SetTitle(title_complete.c_str());
  
  //saving into the .root output file
  outFile->cd();
  
  h_2D_eff->Write((configtree.get<std::string>("options.out_name") + "_eff").c_str());
  
  //write the average efficiency in the out log
  double gen = h_2D_gen->GetEntries();
  double reco = h_2D_reco->GetEntries() * scaling;
  double errstat_eff = sqrt(reco * (gen - reco) / pow(gen, 3.));  //simple Binomial error
  
  log_stream << "efficiency = " << (reco/gen) << " +- " << errstat_eff << " (stat) " 
             << "(" << (errstat_eff/(reco/gen))*100 << ")"<< std::endl;
  
  //cleaning of memory
  delete h_2D_eff;
  
  return;
}
