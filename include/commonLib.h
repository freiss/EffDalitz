#ifndef INCLUDE_COMMONLIB_H
#define INCLUDE_COMMONLIB_H 1

/*!
 *  @file      commonLib.h
 *  @author    Alessio Piucci
 *  @brief     Common library of methods used by different macros and structures.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include "TROOT.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"

using namespace std;

//parse a string containing some environment variables
std::string ParseEnvName(std::string input_string);

//compute the binomial error, mainly for efficiency computation
double BinomialError(int passed, int tries);

//compute the error of a generic ratio
double ErrorRatio(double num, double num_err, double den, double den_err);

//search and fix for entry errors for a TEfficiency, TH2D
void FixTEfficiencyEntries(TH2D* h_pass, TH2D* h_total, unsigned int &num_fixed_bins);

//search and fix for entry errors for a TEfficiency, TH3D
void FixTEfficiencyEntries(TH3D* h_pass, TH3D* h_total, unsigned int &num_fixed_bins);

//fix weights problems for a TEfficiency, TH2D
void FixTEfficiencyWeights(TH2D* h_pass, TH2D* h_total);

#endif // INCLUDE_COMMONLIB_H
