/*!
 *  @file      effLUT.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to retrieve the efficiency value for a given candidate, reading from lookup tables.
 *  @return    Returns the efficiency value.                   
 */

#include "effLUT.h"

//----------------------------------------------
// Get efficiency as function of full kinematics of the event
//----------------------------------------------
void effLUT::GetEventEff(const double Kin_1, const double Kin_2,
                         const double BDTpt_1, const double BDTfd_1,
                         const double BDTpt_2, const double BDTfd_2,
                         const double PIDp, const double PIDeta, const unsigned int nTracks,
                         double KinEff_tot[], double KinEff_1[], double KinEff_2[],
                         double BDTEff_tot[], double BDTEff_1[], double BDTEff_2[],
                         double PIDEff[],
                         double comb_eff[]){
  
  //number of total [0] and passed [1] events
  int kin_events_1[2] = {-1};
  int kin_events_2[2] = {-1};
  int BDT_events_1[2] = {-1};
  int BDT_events_2[2] = {-1};
  int PID_events[2] = {-1};
  
  //----------------------------//
  //  compute the efficiencies  //
  //----------------------------//
  
  //kinematic efficiency
  GetKineEff(Kin_1, Kin_2,
             KinEff_tot, KinEff_1, KinEff_2,
             kin_events_1, kin_events_2);
  
  //BDT efficiency
  GetBDTEff(BDTpt_1, BDTfd_1, BDTpt_2, BDTfd_2,
            BDTEff_tot, BDTEff_1, BDTEff_2,
            BDT_events_1, BDT_events_2);
  
  //PID efficiency
  GetPIDEff(PIDp, PIDeta, nTracks, PIDEff, PID_events);

  //---------------------------------------------//
  //  some checks for the computed efficiencies  //
  //---------------------------------------------//
  
  bool eff_ok = true;
  
  //some checks of the efficiency, error and number of passed/total events
  //if there are some problems, set the involved efficiency and error to -1.
  //and return the value to the calling function
  
  //check kinematic efficiency
  if(std::isnan(KinEff_tot[0]) || (KinEff_tot[0] <= 0.) || (KinEff_tot[0] > 1.)
     || std::isnan(KinEff_tot[1]) || (KinEff_tot[1] < 0.) || (KinEff_tot[1] > 1.)
     || (kin_events_1[0] < 0.) || (kin_events_2[0] < 0.)){
    KinEff_tot[0] = -1.;
    KinEff_tot[1] = -1.;
   
    //std::cout << "Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
    //          << ", kin_events_total_1 = " << kin_events_1[0] << ", kin_events_total_2 = " << kin_events_2[0]
    //          << ", kin_eff = " << KinEff_tot[0] << " +- " << KinEff_tot[1] << std::endl;
    
    eff_ok = false;
  }
  
  //check BDT efficiency
  if(std::isnan(BDTEff_tot[0]) || (BDTEff_tot[0] <= 0.) || (BDTEff_tot[0] > 1.)
     || std::isnan(BDTEff_tot[1]) || (BDTEff_tot[1] < 0.) || (BDTEff_tot[1] > 1.)
     || (BDT_events_1[0] < 0.) || (BDT_events_2[0] < 0.)){
    BDTEff_tot[0] = -1.;
    BDTEff_tot[1] = -1.;
    
    std::cout << "BDTpt_1 = " << BDTpt_1 << ", BDTfd_1 = " << BDTfd_1
              << ", BDTpt_2 = " << BDTpt_2 << ", BDTfd_2 = " << BDTfd_2
              << ", BDT_events_total_1 = " << BDT_events_1[0] << ", BDT_events_passed_1 = " << BDT_events_1[1]
              << ", BDT_events_total_2 = " << BDT_events_2[0] << ", BDT_events_passed_2 = " << BDT_events_2[1]
              << ", BDT_eff[0] = " << BDTEff_tot[0] << " +- " << BDTEff_tot[1] << std::endl;
    
    eff_ok = false;
  }
  
  //check PID efficiency
  if(std::isnan(PIDEff[0]) || (PIDEff[0] <= 0.) || (PIDEff[0] > 1.)
     || std::isnan(PIDEff[1]) || (PIDEff[1] < 0.) || (PIDEff[1] > 1.)
     || (PID_events[0] < 0.)){
    PIDEff[0] = -1.;
    PIDEff[1] = -1.;
    
    std::cout << "PIDp = " << PIDp << ", PIDeta = " << PIDeta << ", nTracks = " << nTracks
              << ", PID_events_total = " << PID_events[0] << ", PID_events_passed = " << PID_events[1]
              << ", PID_eff = " << PIDEff[0] << " +- " << PIDEff[1] << std::endl;
    
    eff_ok = false;
  }
  
  if(!eff_ok){
    comb_eff[0] = -1.;
    comb_eff[1] = -1.;
    
    return;
  }  //if(!eff_ok)
  
  //--------------------------------//
  //  now combine the efficiencies  //
  //--------------------------------//
  
  //now I fill two arrays, with total and passed events,
  //to compute the combined efficiency
  int total[5] = {-1};
  int passed[5] = {-1};
  
  //I fill the arrays only with the enabled efficiencies 
  //for this purpose, I also use an helper index i
  unsigned int i = 0;
  
  if(kin1_eff_on){
    total[i] = kin_events_1[0];
    passed[i] = kin_events_1[1];
    
    ++i;
  }
  
  if(kin2_eff_on){
    total[i] = kin_events_2[0];
    passed[i] = kin_events_2[1];
    
    ++i;
  }
  
  if(BDT1_eff_on){
    total[i] = BDT_events_1[0];
    passed[i] = BDT_events_1[1];
    ++i;
  }

  if(BDT2_eff_on){
    total[i] = BDT_events_2[0];
    passed[i] = BDT_events_2[1];
    
    ++i; 
  }
  
  if(PID_eff_on){
    total[i] = BDT_events_2[0];
    passed[i] = BDT_events_2[1];
    
    ++i;
  }
  
  //finally, I can compute the combined efficiencies
  //note that at this stage the index i is equal to the number of enabled efficiencies
  GetCombinedEff(total, passed, i,
                 KinEff_tot, BDTEff_tot, PIDEff,
                 comb_eff);
  
  return;
}



//---------------------------------------------------------
// Combine kinematic, BDT and PID efficiencies
//---------------------------------------------------------
void effLUT::GetCombinedEff(int total[], int passed[], unsigned num_eff,
                            double KinEff[], double BDTEff[], double PIDEff[],
                            double comb_eff[]){
  
  //are the TEfficiency distributions compatible with the bayesian combination of the TEfficiency::Combine method?
  if(bayesian_combine){
    double up_error, low_error;
    
    //this method computes both efficiency and errors
    comb_eff[0] = TEfficiency::Combine(up_error, low_error,
                                       num_eff, passed, total,
                                       alpha, beta, conf_level);
    
    //for now, the error is the simple average of the upper and lower ones
    comb_eff[1] = (up_error + low_error)/2.;
  }
  else
  {
    //if I cannot combine the efficiencies in the most proper way,
    //the simple multiplication and error propagation is enough for now...
    
    //total efficiency
    comb_eff[0] = KinEff[0] * BDTEff[0] * PIDEff[0];
    
    //error of the total efficiency
    comb_eff[1] = sqrt(pow(BDTEff[0]*PIDEff[0]*KinEff[1], 2.)
                       + pow(KinEff[0]*PIDEff[0]*BDTEff[1], 2.)
                       + pow(KinEff[0]*BDTEff[0]*PIDEff[1], 2.));
  }
  
  return;
}


//---------------------------------------------------------
// Get a single efficiency from a 2D TEfficiency object, with number of total and passed events
//---------------------------------------------------------
void effLUT::GetSingleEff(const double var_1, const double var_2,
                          TEfficiency *h_eff, double Eff[], int events[]){
  
  //find bin for these coordinates, but first check if the ranges are ok
  if( (var_1 < h_eff->GetTotalHistogram()->GetXaxis()->GetXmin())
      || (h_eff->GetTotalHistogram()->GetXaxis()->GetXmax() < var_1)
      || (var_2 < h_eff->GetTotalHistogram()->GetYaxis()->GetXmin())
      || (h_eff->GetTotalHistogram()->GetYaxis()->GetXmax() < var_2)
     ){
    std::cout << "Warning: values of efficiency out of histos. Setting dummy efficiency." << std::endl;
    
    //dummy values
    Eff[0] = -1.;
    Eff[1] = -1.;
    
    return;
  }
  
  // Find bin for these coordinates
  int i_bin = h_eff->FindFixBin(var_1, var_2);
  
  //efficiency
  Eff[0] = h_eff->GetEfficiency(i_bin);
  
  //efficiency error
  Eff[1] = (h_eff->GetEfficiencyErrorUp(i_bin) + h_eff->GetEfficiencyErrorLow(i_bin))/2.;
  
  //total events
  events[0] = h_eff->GetTotalHistogram()->GetBinContent(i_bin);
  
  //passed events
  events[1] = h_eff->GetPassedHistogram()->GetBinContent(i_bin);
  
  //std::cout << "h_eff->GetEfficiency(i_bin) = " << h_eff->GetEfficiency(i_bin)
  //          << "h_eff->GetEfficiencyErrorUp(i_bin) = " << h_eff->GetEfficiencyErrorUp(i_bin)
  //          << "h_eff->GetEfficiencyErrorLow(i_bin) = " << h_eff->GetEfficiencyErrorLow(i_bin) << std::endl;
  
  return;
}

//---------------------------------------------------------
// Get efficiency of kinematic selection
// from two different 2D efficiency distributions
//---------------------------------------------------------
void effLUT::GetKineEff(const double Kin_1, const double Kin_2,
                        double KinEff_tot[], double KinEff_1[], double KinEff_2[],
                        int kin_events_1[], int kin_events_2[]){
  
  //is the first kinematic efficiency switched on?
  if(kin1_eff_on)
    GetSingleEff(Kin_1, Kin_2, h_kin_eff_1, KinEff_1, kin_events_1);
  else
  {
    KinEff_1[0] = 1.;  //dummy efficiency
    KinEff_1[1] = 0.;  //dummy error
    
    //dummy number of events
    kin_events_1[0] = 0;
    kin_events_1[1] = -1;
  }
  
  //is the first kinematic efficiency switched on?
  if(kin2_eff_on)
    GetSingleEff(Kin_1, Kin_2, h_kin_eff_2, KinEff_2, kin_events_2);
  else
  {
    KinEff_2[0] = 1.;  //dummy efficiency
    KinEff_2[1] = 0.;  //dummy error
    
    //dummy number of events
    kin_events_2[0] = 0;
    kin_events_2[1] = -1;
  }
  
  //total kinematic efficiency
  KinEff_tot[0] = KinEff_1[0] * KinEff_2[0];
  
  //total kinematic error
  KinEff_tot[1] = sqrt( pow(KinEff_2[0]*KinEff_1[1], 2.)
                        + pow(KinEff_1[0]*KinEff_2[1], 2.) );
  
  return;
}

//-----------------------------------
// Get BDT efficiency for two cuts
//-----------------------------------
void effLUT::GetBDTEff(const double pt_1, const double fd_1,
                       const double pt_2, const double fd_2,
                       double BDTEff_tot[], double BDTEff_1[], double BDTEff_2[],
                       int BDT_events_1[], int BDT_events_2[]){
  
  //are the BDT efficiencies switched on?
  if(BDT1_eff_on)
    GetSingleEff(pt_1, fd_1, h_BDT_eff_1, BDTEff_1, BDT_events_1);
  else
  {
    BDTEff_1[0] = 1.;  //dummy efficiency
    BDTEff_1[1] = 0.;  //dummy error
    
    //dummy number of events
    BDT_events_1[0] = 0;
    BDT_events_1[1] = -1;
  }
  
  //is a second BDT cut enabled?
  if(BDT2_eff_on)
    GetSingleEff(pt_2, fd_2, h_BDT_eff_2, BDTEff_2, BDT_events_2);
  else
  {
    BDTEff_2[0] = 1.;  //dummy efficiency
    BDTEff_2[1] = 0.;  //dummy error 
  
    //dummy number of events
    BDT_events_2[0] = 0;
    BDT_events_2[1] = -1;
  }
  
  //total BDT efficiency
  BDTEff_tot[0] = BDTEff_1[0] * BDTEff_2[0];
  
  //total BDT error
  BDTEff_tot[1] = sqrt( pow(BDTEff_2[0]*BDTEff_1[1], 2.)
                    + pow(BDTEff_1[0]*BDTEff_2[1], 2.) );
  
  return;
}
		
//----------------------------- 
// Get efficiency for a PID cut
//-----------------------------
void effLUT::GetPIDEff(const double p, const double eta, const double nTracks,
                       double PIDEff[], int PID_events[]){
  
  //is the PID efficiency switched on?
  if(!PID_eff_on){
    PIDEff[0] = 1.;  //dummy efficiency
    PIDEff[1] = 0.;  //dummy error
    
    //dummy number of events
    PID_events[0] =  0;
    PID_events[1] = -1;
    
    return;
  }
  
  //find bin for these coordinates, but first check if the ranges are ok
  if( (p < h_PID_eff->GetTotalHistogram()->GetXaxis()->GetXmin())
     || (h_PID_eff->GetTotalHistogram()->GetXaxis()->GetXmax() < p)
     || (eta < h_PID_eff->GetTotalHistogram()->GetYaxis()->GetXmin())
     || (h_PID_eff->GetTotalHistogram()->GetYaxis()->GetXmax() < eta)
     || (nTracks < h_PID_eff->GetTotalHistogram()->GetZaxis()->GetXmin())
     || (h_PID_eff->GetTotalHistogram()->GetZaxis()->GetXmax() < nTracks)
     ){
    std::cout << "Warning: PID kinematic values out of histos. Setting dummy efficiency." << std::endl;
    
    //dummy values
    PIDEff[0] = -1.;
    PIDEff[1] = -1.;
    
    return;
  }
  
	int i_bin = h_PID_eff->FindFixBin(p, eta, nTracks);
  
  //efficiency
  PIDEff[0] = h_PID_eff->GetEfficiency(i_bin);
  
  //error
  PIDEff[1] = (h_PID_eff->GetEfficiencyErrorUp(i_bin) + h_PID_eff->GetEfficiencyErrorLow(i_bin))/2.;

  //total events
  PID_events[0] = h_PID_eff->GetTotalHistogram()->GetBinContent(i_bin);
  
  //passed events
  PID_events[1] = h_PID_eff->GetPassedHistogram()->GetBinContent(i_bin);
  
  //std::cout << "h_PID_eff->GetEfficiencyErrorUp(i_bin) = " << h_PID_eff->GetEfficiencyErrorUp(i_bin)
  //          << "h_PID_eff->GetEfficiencyErrorLow(i_bin) = " << h_PID_eff->GetEfficiencyErrorLow(i_bin) << std::endl;
  
  return;
}

//-----------------------------
// Get the mean efficiency from a TEfficiency object
//-----------------------------
void effLUT::GetMeanEff(TEfficiency *h_eff, double Eff[]){
  
  double num = h_eff->GetPassedHistogram()->GetEntries();
  double denum = h_eff->GetTotalHistogram()->GetEntries();
  
  Eff[0] = num/denum;
  Eff[1] = BinomialError(num, denum);
  
  return;
}

//----------------------------- 
// Get the mean efficiencies    
//-----------------------------                                                                                                                     
void effLUT::GetMeanEfficiencies(double KinEff_1[], double KinEff_2[],
                                 double BDTEff_1[], double BDTEff_2[],
                                 double PIDEff[], double triggerEff[],
                                 double recoEff[], double selectionEff[]){
  
  //get kinematic efficiencies
  if(kin1_eff_on)
    GetMeanEff(h_kin_eff_1, KinEff_1);
  
  if(kin2_eff_on)
    GetMeanEff(h_kin_eff_2, KinEff_2);
  
  //get BDT efficiencies
  if(BDT1_eff_on)
    GetMeanEff(h_BDT_eff_1, BDTEff_1);
  
  if(BDT2_eff_on)
    GetMeanEff(h_BDT_eff_2, BDTEff_2);
  
  //get PID efficiency
  if(PID_eff_on)
    GetMeanEff(h_PID_eff, PIDEff);
  
  //get single kinematic contributions
  if(SingleKinComponents_eff_on){
    GetMeanEff(h_trigger_eff, triggerEff);
    GetMeanEff(h_reco_eff, recoEff);
    GetMeanEff(h_selection_eff, selectionEff);
  }
  
  return;
}

//----------------------------------------------
// Get single components of the kinematic efficiency
//----------------------------------------------
void effLUT::GetKinComponents(const double Kin_1, const double Kin_2,
                              double TriggerEff[], double RecoEff[], double SelectionEff[]){
  
  //if it's switched off, return unvalid efficiencies
  if(!SingleKinComponents_eff_on){
    TriggerEff[0] = -1.;
    TriggerEff[1] = -1.;
    
    RecoEff[0] = -1.;
    RecoEff[1] = -1.;
    
    SelectionEff[0] = -1.;
    SelectionEff[1] = -1.;
    
    return;
  } 
    
  //number of total [0] and passed [1] events
  int events[2] = {-1};
  
  GetSingleEff(Kin_1, Kin_2, h_trigger_eff, TriggerEff, events);
  
  if(std::isnan(TriggerEff[0]) || (TriggerEff[0] <= 0.) || (TriggerEff[0] > 1.)
     || std::isnan(TriggerEff[1]) || (TriggerEff[1] < 0.) || (TriggerEff[1] > 1.)
     || (events[0] < 0.) || (events[0] < 0.)){
    TriggerEff[0] = -1.;
    TriggerEff[1] = -1.;
  }
  
  GetSingleEff(Kin_1, Kin_2, h_reco_eff, RecoEff, events);
  
  if(std::isnan(RecoEff[0]) || (RecoEff[0] <= 0.) || (RecoEff[0] > 1.)
     || std::isnan(RecoEff[1]) || (RecoEff[1] < 0.) || (RecoEff[1] > 1.)
     || (events[0] < 0.) || (events[0] < 0.)){
    RecoEff[0] = -1.;
    RecoEff[1] = -1.;
  }
  
  GetSingleEff(Kin_1, Kin_2, h_selection_eff, SelectionEff, events);
  
  if(std::isnan(SelectionEff[0]) || (SelectionEff[0] <= 0.) || (SelectionEff[0] > 1.)
     || std::isnan(SelectionEff[1]) || (SelectionEff[1] < 0.) || (SelectionEff[1] > 1.)
     || (events[0] < 0.) || (events[0] < 0.)){
    SelectionEff[0] = -1.;
    SelectionEff[1] = -1.;
  }
  
  return;
}


//------------
// Constructor
//------------
effLUT::effLUT(const std::string _configFileName)
  : configFileName(_configFileName) {
  
  kinEffFile_1 = NULL;
  h_kin_eff_1 = NULL;
  
  kinEffFile_2 = NULL;
  h_kin_eff_2 = NULL;
  
  bdtEffFile_1 = NULL;
  bdtEffFile_2 = NULL;
  
  h_BDT_eff_1 = NULL;
  h_BDT_eff_2 = NULL;
  
  pidEffFile = NULL;
  PIDpassed = NULL;
  PIDtotal = NULL;
    
  h_PID_eff = NULL;
  
  //-------------------------------//
  //  read the configuration file  //
  //-------------------------------//
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  //set which efficiencies are enabled
  kin1_eff_on = (bool) configtree.get<unsigned int>("kinEff.KinEff_1");
  kin2_eff_on = (bool) configtree.get<unsigned int>("kinEff.KinEff_2");
  BDT1_eff_on = (bool) configtree.get<unsigned int>("BDTEff.BDTEff_1");
  BDT2_eff_on = (bool) configtree.get<unsigned int>("BDTEff.BDTEff_2");
  PID_eff_on = (bool) configtree.get<unsigned int>("PIDEff.PIDEff");
  
  SingleKinComponents_eff_on = (bool) configtree.get<unsigned int>("kinEff.singleComp.status");
  
  //--------------------------------------------------------//
  //  load selection efficiency as function of Dalitz plot  //
  //--------------------------------------------------------//
  
  std::string input_filename;

  //is the first kinematic cut to apply?
  if(kin1_eff_on){
    input_filename = configtree.get<std::string>("kinEff.inFile_1");
    
    kinEffFile_1 = TFile::Open(input_filename.c_str(), "READ");
    
    if(kinEffFile_1 == NULL){
      std::cout<< "Kin_1 file not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading Kin_1 LUT from: " << input_filename << std::endl;
      
    //open histo with efficiency
    h_kin_eff_1 = (TEfficiency*) kinEffFile_1->Get((configtree.get<std::string>("kinEff.inHisto_1")).c_str());
      
    if(h_kin_eff_1 == NULL){
      std::cout << "Kin_1 histo not found" << std::endl;
      exit(EXIT_FAILURE);
    }
  }  //if(kin1_eff_on)
    
  ////////
    
  //is the second kinematic cut to apply?
  if(kin2_eff_on){
    input_filename = configtree.get<std::string>("kinEff.inFile_2");
      
    kinEffFile_2 = TFile::Open(input_filename.c_str(), "READ");
    
    std::cout << "Loading efficiency LUT from: " << input_filename << std::endl;
    
    if(kinEffFile_2 == NULL){
      std::cout<< "Kin_2 file not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open histo with efficiency
    h_kin_eff_2 = (TEfficiency*) kinEffFile_2->Get((configtree.get<std::string>("kinEff.inHisto_2")).c_str());
      
    if(h_kin_eff_2 == NULL){
      std::cout << "Kin_2 histo not found" << std::endl;
      exit(EXIT_FAILURE);
    }
  }  //if(kin2_eff_on)
  
  
  //---------------------------//
  //  load BDT reference data  //
  //---------------------------//
  
  //is the first BDT cut to apply?
  if(BDT1_eff_on){
    
    input_filename = configtree.get<std::string>("BDTEff.inFile_1");
    
    bdtEffFile_1 = TFile::Open(input_filename.c_str(), "READ");
    
    if(bdtEffFile_1 == NULL){
      std::cout << "BDT_1 file not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading BDT_1 efficiency LUT from: " << input_filename << std::endl;
      
    //open histo with efficiency
    TH3D *histo_BDT = (TH3D*) bdtEffFile_1->Get((configtree.get<std::string>("BDTEff.inHisto_1")).c_str());
    
    if(histo_BDT == NULL){
      std::cout << "BDT_1 histo not found." << std::endl;
      exit(EXIT_FAILURE);
    }
      
    //retrieve BDT cut
    bdtcut_1 = configtree.get<double>("BDTEff.bdtcut_1");
    
    std::cout << "bdtcut_1 = " << bdtcut_1 << std::endl;
    
    //now I need to create two TH2D to fill the TEfficiency,
    //one with all events, the other with passed ones
    
    //Project3D rocks!!
    TH2D *h_BDT_total = (TH2D*) histo_BDT->Project3D("yx_1");
    
    std::cout << "h_BDT_total->GetXaxis()->GetTitle() = " << h_BDT_total->GetXaxis()->GetTitle() << std::endl;
    std::cout << "h_BDT_total->GetYaxis()->GetTitle() = " << h_BDT_total->GetYaxis()->GetTitle() << std::endl;
    
    //to select only passed events, I set the range of the TH3D histo,
    //and then I project it again
    histo_BDT->GetZaxis()->SetRangeUser(bdtcut_1, histo_BDT->GetZaxis()->GetXmax());
    
    TH2D *h_BDT_passed = (TH2D*) histo_BDT->Project3D("yx_2");
    
    std::cout << "h_BDT_passed->GetXaxis()->GetTitle() = " << h_BDT_passed->GetXaxis()->GetTitle() << std::endl;
    std::cout << "h_BDT_passed->GetYaxis()->GetTitle() = " << h_BDT_passed->GetYaxis()->GetTitle() << std::endl;
    
    //the TEfficiency doesn't like not empty underflow and overflow bins                                                                                   
    h_BDT_total->ClearUnderflowAndOverflow();
    h_BDT_passed->ClearUnderflowAndOverflow();

    //number of bins which were fixed, to use the TEfficiency                                                                                      
    unsigned int num_fixed_bins = 0;
    
    //check consistency of generated and reco histograms
    if(!TEfficiency::CheckConsistency(*h_BDT_passed, *h_BDT_total)){
      std::cout << "Error: passed and total BDT histos are not consistent!" << std::endl;
      
      FixTEfficiencyEntries(h_BDT_passed, h_BDT_total, num_fixed_bins);
      FixTEfficiencyWeights(h_BDT_passed, h_BDT_total);
      
      //exit(EXIT_FAILURE);
    }
    
    std::cout << "num_fixed_bins = " << num_fixed_bins
              << " (" << (num_fixed_bins/(double) h_BDT_passed->GetNcells())*100 << "%)" << std::endl;
    
    h_BDT_eff_1 = new TEfficiency(*h_BDT_passed, *h_BDT_total);
    
    //use Wilson statistic, please don't ask me but use this reference:
    //https://root.cern.ch/doc/master/classTEfficiency.html
    h_BDT_eff_1->SetConfidenceLevel(0.683);
    h_BDT_eff_1->SetStatisticOption(TEfficiency::kFWilson);
    
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  ////////
    
  //is the second BDT cut to apply?
  if(BDT2_eff_on){
    
    input_filename = configtree.get<std::string>("BDTEff.inFile_2");
    
    bdtEffFile_2 = TFile::Open(input_filename.c_str(), "READ");
    
    if(bdtEffFile_2 == NULL){
      std::cout << "BDT_2 file not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading BDT_2 efficiency LUT from: " << input_filename << std::endl;
      
    //open histo with data
    TH3D *histo_BDT = (TH3D*) bdtEffFile_2->Get((configtree.get<std::string>("BDTEff.inHisto_2")).c_str());
      
    if(histo_BDT == NULL){
      std::cout << "BDT_2 histo not found." << std::endl;
      exit(EXIT_FAILURE);
    }
      
    //retrieve BDT cut
    bdtcut_2 = configtree.get<double>("BDTEff.bdtcut_2");
      
    std::cout << "bdtcut_2 = " << bdtcut_2 << std::endl;

    //now I need to create two TH2D to fill the TEfficiency,
    //one with all events, the other with passed ones
    
    //Project3D rocks!!
    TH2D *h_BDT_total = (TH2D*) histo_BDT->Project3D("yx_3");
    
    std::cout << "h_BDT_total->GetXaxis()->GetTitle() = " << h_BDT_total->GetXaxis()->GetTitle() << std::endl;
    std::cout << "h_BDT_total->GetYaxis()->GetTitle() = " << h_BDT_total->GetYaxis()->GetTitle() << std::endl;
    
    //to select only passed events, I set the range of the TH3D histo,
    //and then I project it again
    histo_BDT->GetZaxis()->SetRangeUser(bdtcut_2, histo_BDT->GetZaxis()->GetXmax());
    
    TH2D *h_BDT_passed = (TH2D*) histo_BDT->Project3D("yx_4");
    
    std::cout << "h_BDT_passed->GetXaxis()->GetTitle() = " << h_BDT_passed->GetXaxis()->GetTitle() << std::endl;
    std::cout << "h_BDT_passed->GetYaxis()->GetTitle() = " << h_BDT_passed->GetYaxis()->GetTitle() << std::endl;
    
    //the TEfficiency doesn't like not empty underflow and overflow bins
    h_BDT_total->ClearUnderflowAndOverflow();
    h_BDT_passed->ClearUnderflowAndOverflow();
    
    //number of bins which were fixed, to use the TEfficiency
    unsigned int num_fixed_bins = 0;
    
    //check consistency of generated and reco histograms
    if(!TEfficiency::CheckConsistency(*h_BDT_passed, *h_BDT_total)){
      std::cout << "Error: passed and total BDT histos are not consistent!" << std::endl;
      
      FixTEfficiencyEntries(h_BDT_passed, h_BDT_total, num_fixed_bins);
      FixTEfficiencyWeights(h_BDT_passed, h_BDT_total);
      
      //exit(EXIT_FAILURE);
    }
    
    std::cout << "num_fixed_bins = " << num_fixed_bins
              << " (" << (num_fixed_bins/(double) h_BDT_passed->GetNcells())*100 << "%)" << std::endl;
    
    h_BDT_eff_2 = new TEfficiency(*h_BDT_passed, *h_BDT_total);
    
    //use Wilson statistic, please don't ask me but use this reference:
    //https://root.cern.ch/doc/master/classTEfficiency.html
    h_BDT_eff_2->SetConfidenceLevel(0.683);
    h_BDT_eff_2->SetStatisticOption(TEfficiency::kFWilson);
    
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  
  //---------------------------------//
  //  loading PIDEff reference data  //
  //---------------------------------//
  
  if(PID_eff_on){
    
    input_filename = configtree.get<std::string>("PIDEff.inFile");
    
    pidEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(pidEffFile != NULL){
      
      std::cout << "Loading PID efficiency LUT from: " << input_filename << std::endl;
      
      //open histo with passed events
      PIDpassed = (TH3D*) pidEffFile->Get((configtree.get<std::string>("PIDEff.inHistoPassed")).c_str());
      
      //open histo with all events
      PIDtotal = (TH3D*) pidEffFile->Get((configtree.get<std::string>("PIDEff.inHistoTotal")).c_str());
      
      if((PIDpassed == NULL) || (PIDtotal == NULL)){
        std::cout << "Error: PID histograms not found!" << std::endl;
        exit(EXIT_FAILURE);
      }
      else
        std::cout << "PID efficiencies loaded." << std::endl;
      
      //the TEfficiency doesn't like not empty underflow and overflow bins
      PIDtotal->ClearUnderflowAndOverflow();
      PIDpassed->ClearUnderflowAndOverflow();
      
      //number of bins which were fixed, to use the TEfficiency
      unsigned int num_fixed_bins = 0;
      
      //check consistency of generated and reco histograms
      if(!TEfficiency::CheckConsistency(*PIDpassed, *PIDtotal)){
        std::cout << "Error: passed and total PID histos are not consistent!" << std::endl;
        
        FixTEfficiencyEntries(PIDpassed, PIDtotal, num_fixed_bins);
        
        //exit(EXIT_FAILURE);
      }
      
      std::cout << "num_fixed_bins = " << num_fixed_bins
                << " (" << (num_fixed_bins/(double) PIDpassed->GetNcells())*100 << "%)" << std::endl;
      
      h_PID_eff = new TEfficiency(*PIDpassed, *PIDtotal);
      
      //use Wilson statistic, please don't ask me but use this reference:
      //https://root.cern.ch/doc/master/classTEfficiency.html
      h_PID_eff->SetConfidenceLevel(0.683);
      h_PID_eff->SetStatisticOption(TEfficiency::kFWilson);
    }
    else
      std::cerr<< "PID Efficiency parameterization file not found." << std::endl;
    
  }  //if((bool) configtree.get<std::string>("kinEff.PIDEff"))
  
  //set the single kinematic components
  if(SingleKinComponents_eff_on){
    
    //trigger component
    input_filename = configtree.get<std::string>("kinEff.singleComp.trigger_file");
    
    TriggerEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(TriggerEffFile == NULL){
      std::cout<< "TriggerEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading TriggerEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_trigger_eff = (TEfficiency*) TriggerEffFile->Get((configtree.get<std::string>("kinEff.singleComp.trigger_histo")).c_str());
    
    if(h_trigger_eff == NULL){
      std::cout << "h_trigger_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //reconstruction + stripping component
    input_filename = configtree.get<std::string>("kinEff.singleComp.reco_file");
    
    RecoEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(RecoEffFile == NULL){
      std::cout<< "RecoEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading RecoEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_reco_eff = (TEfficiency*) RecoEffFile->Get((configtree.get<std::string>("kinEff.singleComp.reco_histo")).c_str());
    
    if(h_reco_eff == NULL){
      std::cout << "h_reco_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //selection
    input_filename = configtree.get<std::string>("kinEff.singleComp.selection_file");
    
    SelectionEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(SelectionEffFile == NULL){
      std::cout<< "SelectionEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading SelectionEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_selection_eff = (TEfficiency*) SelectionEffFile->Get((configtree.get<std::string>("kinEff.singleComp.selection_histo")).c_str());
    
    if(h_selection_eff == NULL){
      std::cout << "h_selection_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
  }  //if(SingleKinComponents_eff_on)
  
  
  //---------------------------------------------------//
  //  now I set the stuff to combine the efficiencies  //
  //---------------------------------------------------//
  
  //I have to check that the TEfficiency distributions are suited
  //to be used with the TEfficiency::Combine method
  
  //to better understand these tests, read the TEfficiency::Combine implementation
  //from the ROOT reference
  
  bayesian_combine = false;
  alpha = -1;
  beta = -1;
  conf_level = -1.;
  
  //all alpha and beta parameters of the distributions must be the same
  if(kin1_eff_on){
    
    //to check if it's bayesian
    bayesian_combine = h_kin_eff_1->UsesBayesianStat();
    
    //check if it's bayesian
    if(bayesian_combine){
      alpha = h_kin_eff_1->GetBetaAlpha();
      beta = h_kin_eff_1->GetBetaBeta();
      conf_level = h_kin_eff_1->GetConfidenceLevel();
    }
  }  //if(kin1_eff_on)
  
  //check the TEfficiency only if the bayesian_combine is still valid
  if(kin2_eff_on && bayesian_combine){
    
    //to check if it's bayesian
    bayesian_combine = h_kin_eff_2->UsesBayesianStat();
    
    if(bayesian_combine){
      
      //in case the previous TEfficiency object was disabled
      if((alpha < 1.) || (beta < 1.)){
        alpha = h_kin_eff_2->GetBetaAlpha();
        beta = h_kin_eff_2->GetBetaBeta();
      }
      else
      {
        if((alpha != h_kin_eff_2->GetBetaAlpha())
           || (beta != h_kin_eff_2->GetBetaBeta())
           || (conf_level != h_kin_eff_2->GetConfidenceLevel()))
          bayesian_combine = false;
      }
    }  //if(bayesian_combine)
  }  //if(kin2_eff_on && bayesian_combine)
  
  //check the TEfficiency only if the bayesian_combine is still valid
  if(BDT1_eff_on && bayesian_combine){
    
    //to check if it's bayesian
    bayesian_combine = h_BDT_eff_1->UsesBayesianStat();
    
    if(bayesian_combine){
      //in case the previous TEfficiency object was disabled
      if((alpha < 1.) || (beta < 1.)){
        alpha = h_BDT_eff_1->GetBetaAlpha();
        beta = h_BDT_eff_1->GetBetaBeta();  
      }
      else
      {
        if((alpha != h_BDT_eff_1->GetBetaAlpha())
           || (beta != h_BDT_eff_1->GetBetaBeta())
           || (conf_level != h_BDT_eff_1->GetConfidenceLevel()))
          bayesian_combine = false;
      }
    }  //if(bayesian_combine)
  }  //if(BDT1_eff_on && bayesian_combine)
  
  //check the TEfficiency only if the bayesian_combine is still valid
  if(BDT2_eff_on && bayesian_combine){
    
    //to check if it's bayesian
    bayesian_combine = h_BDT_eff_2->UsesBayesianStat();
    
    if(bayesian_combine){
      //in case the previous TEfficiency object was disabled
      if((alpha < 1.) || (beta < 1.)){
        alpha = h_BDT_eff_2->GetBetaAlpha();
        beta = h_BDT_eff_2->GetBetaBeta();
      }
      else
      {
        if((alpha != h_BDT_eff_2->GetBetaAlpha())
           || (beta != h_BDT_eff_2->GetBetaBeta())
           || (conf_level != h_BDT_eff_2->GetConfidenceLevel()))
          bayesian_combine = false;
      }
    }  //if(bayesian_combine)
  }  //if(BDT2_eff_on && bayesian_combine)
  
  //check the TEfficiency only if the bayesian_combine is still valid
  if(PID_eff_on && bayesian_combine){
    
    //to check if it's bayesian
    bayesian_combine = h_PID_eff->UsesBayesianStat();
    
    if(bayesian_combine){
      //in case the previous TEfficiency object was disabled
      if((alpha < 1.) || (beta < 1.)){
        alpha = h_PID_eff->GetBetaAlpha();
        beta = h_PID_eff->GetBetaBeta();
      }
      else
      {
        if((alpha != h_PID_eff->GetBetaAlpha())
           || (beta != h_PID_eff->GetBetaBeta())
           || (conf_level != h_PID_eff->GetConfidenceLevel()))
          bayesian_combine = false;
      }
    }  //if(bayesian_combine)
  }  //if(PID_eff_on && bayesian_combine)
  
  if(bayesian_combine)
    std::cout << "Efficiency distributions compatible with Bayesian combination." << std::endl;
  else
    std::cout << "Efficiency distributions NOT compatible with Bayesian combination." << std::endl;
  
}

//-----------
// Destructor
//-----------
effLUT::~effLUT(){
  if(kinEffFile_1 != NULL)
    kinEffFile_1->Close();
  
  if(kinEffFile_2 != NULL)
    kinEffFile_2->Close();
  
  if(bdtEffFile_1 != NULL)
    bdtEffFile_1->Close();
  
  if(bdtEffFile_2 != NULL)
    bdtEffFile_2->Close();
  
  if(pidEffFile != NULL)
    pidEffFile->Close();
}
