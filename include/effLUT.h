#ifndef INCLUDE_EFFLUT_H 
#define INCLUDE_EFFLUT_H 1

/** @class effLUT effLUT.h include/effLUT.h               
 *                                                  
 *                                                                   
 *  @author   Alessio Piucci                                              
 *  @date     2016-03-15                                             
 *  @brief    Script to retrieve the efficiency value for a given candidate, reading from lookup tables.             
 *  @return   Returns the efficiency value.                                                               
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <utility>   //to use std::pair

#include "commonLib.h"

//ROOT libraries
#include "TString.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

class effLUT {

public: 
  
  /// Standard constructor
  effLUT(const std::string _configFileName); 

  ~effLUT( ); ///< Destructor
  
  // get efficiency as function of full kinematics of the event
  void GetEventEff(const double Kin_1, const double Kin_2,
                   const double BDTpt_1, const double BDTfd_1,
                   const double BDTpt_2, const double BDTfd_2,
                   const double PIDp, const double PIDeta, const unsigned int nTracks,
                   double KinEff_tot[], double KinEff_1[], double KinEff_2[],
                   double BDTEff_tot[], double BDTEff_1[], double BDTEff_2[],
                   double PIDEff[], double comb_eff[]);
  
  // get efficiency of kinematic selection from a Dalitz plot
  // with part1-part2 and part2-part3 invariant masses
  void GetKineEff(const double Kin_1, const double Kin_2,
                  double KinEff_tot[], double KinEff_1[], double KinEff_2[], 
                  int kin_events_1[], int kin_events_2[]);
  
  // get BDT efficiency for a single cut
  void GetBDTEff(const double pt_1, const double fd_1,
                 const double pt_2, const double fd_2,
                 double BDTEff_tot[], double BDTEff_1[], double BDTEff_2[],
                 int BDT_events_1[], int BDT_events_2[]);
  
  // get efficiency for a PID cut
  void GetPIDEff(const double p, const double eta, const double nTracks,
                 double PIDEff[], int PID_events[]);

  // get the mean kinematic efficiency
  void GetMeanEff(TEfficiency *h_eff, double Eff[]);
  
  // get the mean efficiencies
  void GetMeanEfficiencies(double KinEff_1[], double KinEff_2[],
                           double BDTEff_1[], double BDTEff_2[],
                           double PIDEff[], double triggerEff[],
                           double recoEff[], double selectionEff[]);
  
  // get a single efficiency from a 2D TEfficiency object, with total of total and passed events
  void GetSingleEff(const double var_1, const double var_2,
                    TEfficiency *h_eff, double Eff[], int events[]);
  
  // combine kinematic, BDT and PID efficiencies
  void GetCombinedEff(int total[], int passed[], unsigned num_eff,
                      double KinEff[], double BDTEff[], double PIDEff[],
                      double comb_eff[]);
  
  // get single components of the kinematic efficiency
  void GetKinComponents(const double Kin_1, const double Kin_2,
                        double TriggerEff[], double RecoEff[], double SelectionEff[]);
    
  // pair of particle names and 4-momenta for BDT and PID cuts
  std::string GetParticleNameBDT(){return configtree.get<std::string>("BDTEff.name_part");};
  std::string GetParticleNamePID(){return configtree.get<std::string>("PIDEff.name_part");};
  
protected:
  
private:

  //to flag which efficiencies are enabled
  bool kin1_eff_on;
  bool kin2_eff_on;
  bool BDT1_eff_on;
  bool BDT2_eff_on;
  bool PID_eff_on;

  bool SingleKinComponents_eff_on;
  
  //variables for the kinematic efficiency, for two cuts
  TFile* kinEffFile_1;        // file containing the kinematic efficiencies as function of the Dalitz plot
  TEfficiency* h_kin_eff_1;   // efficiency as a function of the Dalitz plot
  
  TFile* kinEffFile_2;        // file containing the kinematic efficiencies as function of the Dalitz plot
  TEfficiency* h_kin_eff_2;   // efficiency as a function of the Dalitz plot

  //variables for the single components of the kinematic efficiency
  TFile* TriggerEffFile;
  TEfficiency* h_trigger_eff;
  
  TFile* RecoEffFile;
  TEfficiency* h_reco_eff;
  
  TFile* SelectionEffFile;
  TEfficiency* h_selection_eff;
  
  //variables for the BDT efficiency, for two cuts
  double bdtcut_1;
  TFile* bdtEffFile_1;     // file containing the bdt efficiencies
  TEfficiency *h_BDT_eff_1;  //histo for efficiency
  
  double bdtcut_2;
  TFile* bdtEffFile_2;
  TEfficiency *h_BDT_eff_2;  //histo for efficiency
  
  //variables for the PID efficiency
  TFile* pidEffFile;  // file containing the PID efficiencies
  TH3D* PIDpassed;    // histo with events which passed the PID selection
  TH3D* PIDtotal;     // histo with all events for PID efficiency
  TEfficiency *h_PID_eff;  //histo for efficiency
  
  //config file
  std::string configFileName;
  pt::ptree configtree;
  
  //variables used for combining efficiency
  bool bayesian_combine;
  double alpha;
  double beta;
  double conf_level;
  
};

#endif // INCLUDE_EFFLUT_H
