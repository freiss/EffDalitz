# The macros

## makePlots_Dalitz
The makePlots_Dalitz macro is specific to make 2D Dalitz plots, and it's compatible both for MC and data samples.
It is fully configurable using option files.

It reads kinematic variables from an input tree, and make the desired Dalitz plot.

With the same macro, you can also compute efficiency on Dalitz plot (giving as input an input tree with the MC generated variables), and reweight the plots by 2D histograms containing some kinematic distributions.

## makePlots_2DEff
This macro compute efficiency over 2D distribution, optionally reweighting some 2D input distributions.

## combPlots
The combPlots macro combines 1D or 2D input histograms, using the basic mathematical operations of sum, difference, multiplication and division.
This is useful if you need to combine efficiency distributions.

## signal_yield
The signal_yield macro corrects the signal yield from a fit, with the resulting s-weights and efficiencies. These corrections are applied event-per-event.

This macro uses the effLUT library to compute the event overall efficiency (kinematic, BDT, PID).
The current implementation for the kinematic efficiency it's compatible with a generic 2D efficiency distribution (Dalitz plot, eta vs pt distribution, ...).

# Option files

## makePlots_Dalitz
[Here](config/plotDalitz_MC_Reconstruction.opt) you can find a simple configuration example for the makePlots_Dalitz macro, where:
- some general options are specified;
- some options are set for the efficiency computation;
- only one Dalitz plot named LcD0_D0K is created, specifying the two particle systems syst1 (Lc, D0) and syst2 (D0, K) that you want to use.

Some details about the general options of the example:
- DecayTree: name of the input TTree from which read the variables to make the Dalitz plot;
- cuts: string to specify which cuts you want to use, to select the candidates for the Dalitz plot;
- kinvar_names: names of the px, py, pz, E kinematic variables as they appear in the input tree;
- particles: strings with the names of the three particles that you want to combine to make the Dalitz plot;
- out_name: output name of the plots, saved in the output root file;
- x_axis_label, y_axis_label: x and y labels of the final plots; 
- mother_plot: some options to make a 1D histogram of the mother invariant mass.
- log_file: path and name of the output log file.

Finally some details about the efficiency computation:
- EffOverMC: boolean variable to specify if you want to compute and to make a plot of the efficiency on Dalitz plot, option only valid if you are running over MC samples;
- MCDecayTree: name of the TTree where the MC-truth generated variable are located;
- cuts: string to specify which cuts you want to use, to select the candidates for the Dalitz plot;
- NormalizeNum, NormalizeDen: boolean variables to specify you want to 'normalize' the numerator or denominator using some double values, option only used until now to compute the Generator Level Cut efficiency;
- NormalizeNum_var, NormalizeDen_var: values of the normalization factors for numerator and denominator of the efficiency;
- 2DReweight: boolean variable to switch on/off the efficiency reweight following an input 2D distribution, specified in the Reweight field.

## makePlots_2DEff
[Here](config/plot2DEff_MC.info) you can find a simple configuration example for the makePlots_2DEff macro, where:
- some general options are specified;
- some options are set for the efficiency computation.

Some details about the general options of the example:
- DecayTree: name of the input TTree from which read the variables to make the Dalitz plot;
- cuts: string to specify which cuts you want to use, to select the candidates for the Dalitz plot;
- var1_name, var2_name: names of the two variables that you want to use for the 2D efficiency distribution;
- var_numbins, var_low, var_high: number of bins, lower and higher boundaries for both var1 and var2;
- out_name: output name of the plots, saved in the output root file;
- x_axis_label, y_axis_label: x and y labels of the final plots;
- log_file: path and name of the output log file.

The options related to the efficiency section are the same of the makePlots_Dalitz macro, described above.

## signal_yield
[Here](config/signal_yield.info) you can find a configuration example for the signal_yield macro.

There, you have to specify some options to identify the variables in the dataset, such as the dataset and workspace names, and the name of the s-weights that you want to use for the correction.

You also have to link the effLUT config file that you want to use for the efficiency correction.

If you don't want to use compute the event-efficiencies from look up tables, you can set some dummy efficiencies by enabling the dummy_eff.status variable in the config file, specifying which kinematic, BDT and PID dummy values you want to use.

## effLUT
[Here](config/effLUT.info) you can find an option file to configure the efficiency correction for the signal yield computation.
There, you have to specify the options for the kinematic, BDT and PID efficiencies.

You first have to specify the names of the E and momentum variables as they are in the dataset that you want to correct.

About the kinematic correction:
- KinEff: a boolean to switch on/off the kinematic correction;
- inFile, inHisto: name of the file containing the 2D kinematic efficiency distribution; the 2D histo name has to be specified too; 
- Kin_1_name, Kin_2_name: names of the kinematic variables as they are specified in the dataset, used to retrieve the correct phase space point in the 2D efficiency distribution.

Actually, the effLUT macro is able to handle with two different BDT cuts, on the same or on different particles.
Some specification, for the first BDT cut only:
- BDTEff_1: a boolean to switch on/off the first BDT cut correction;
- inFile_1, inHisto_1: name of the file containing the BDT efficiency distribution; the histo name has to be specified too;
- part_name_1: name of the particle on which you are applying the BDT cut;
- flightdist_name_1: name of the flight distance variable, an input quantity for the BDT efficiency;
- bdtcut_1: cut value for the BDT, only particles with BDT value greater than the cut value will be selected.

Finally, some words about the PID efficiency:
- PIDEff: a boolean to switch on/off the PID efficiency correction;
- inFile, inHistoPassed, inHistoTotal: name of the file containing the reference histos with all particles before and after the PID cut; the histo names have to be specified too;
- part_name_: name of the particle on which you are applying the PID cut;
- ntracks_name: name of the variable corresponding to the number of tracks in the event, an input quantity for the PID efficiency.