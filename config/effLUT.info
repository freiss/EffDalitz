; 14/04/2016 - Alessio Piucci
; Option file for the effLUT
; Lambda_b -> Lambda_c D0 K

; Name of kinematic variables used to fill TLorentzVectors
variables {
       E_name		"_PE"
       px_name          "_PX"
       py_name          "_PY"
       pz_name          "_PZ"
}

; Options for the kinematic efficiency
kinEff {
       ; first kin LUT
       KinEff_1		1

       inFile_1		"$CI_WORKDIR/MCDalitz_GenLevCuts_plots.root"
       inHisto_1	"h2_lambdab_Dalitz_genlevcut_eff"
       
       ; first kin LUT
       KinEff_2         1

       inFile_2         "$CI_WORKDIR/MCDalitz_TriggerRecoStripping_plots.root"
       inHisto_2        "h2_lambdab_Dalitz_TriggerRecoStripping_eff"
       
       ; kinematic variables
       Kin_1_name	"LcD0_M2"
       Kin_2_name	"D0K_M2"

       ; single components of the kinematic variables
       singleComp {
       	    status		1
	    
       	    trigger_file	"$CI_WORKDIR/MCDalitz_Trigger_plots.root"
	    trigger_histo	"h2_lambdab_Dalitz_Trigger_eff"

	    reco_file		"$CI_WORKDIR/MCDalitz_RecoStripping_plots.root"
	    reco_histo		"h2_lambdab_Dalitz_RecoStripping_eff"
	    
	    selection_file	"$CI_WORKDIR/MCDalitz_Selection_plots.root"
	    selection_histo	"h2_lambdab_Dalitz_Selection_eff"
       }
}

; Options for the BDT efficiency
BDTEff {
       ; first cut
       BDTEff_1			1

       inFile_1			"externals/DfromBBDTs/efficiencies/dfromb_effs.root"
       inHisto_1		"D0_NC_sig"
       
       part_name_1		"D0"
       flightdist_name_1  	"D0_FD_ORIVX"
       
       ; select particles with bdt value > bdtcut
       bdtcut_1	         	0.03
       
       ; second cut
       BDTEff_2                 1

       inFile_2                 "externals/DfromBBDTs/efficiencies/dfromb_effs.root"
       inHisto_2                "Lc_NC_sig"

       part_name_2              "Lc"
       flightdist_name_2        "Lc_FD_ORIVX"

       ; select particles with bdt value > bdtcut
       bdtcut_2                 0.07
}

; Options for the PID efficiency on K daughter
PIDEff {
       PIDEff		1
       
       inFile		"root://eoslhcb.cern.ch//eos/lhcb/user/a/apiucci/ganga/Lb2LcD0K/PerfHists_K_V3ProbNNK_0-05_Strip21_MagDown_Custom_scheme_P_ETA_nTracks.root"
       inHistoPassed	"PassedHist_K_V3ProbNNK > 0.05_All__K_P_K_Eta_nTracks"
       inHistoTotal	"TotalHist_K_V3ProbNNK > 0.05_All__K_P_K_Eta_nTracks"

       part_name	"K"
       ntracks_name	"nTracks"
}
