/*!
 *  @file      mean_eff.cpp
 *  @author    Alessio Piucci
 *  @brief     Macro to compute the mean efficiencies from look up tables.
 *  @return    Returns the values of the mean efficiencies.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include "TROOT.h"
#include <TFile.h>
#include "TString.h"

//other libraries
#include "../include/effLUT.cpp"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

int main(int argc, char** argv){
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string outFileName = "";
  std::string configFileName = "";
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "c:o:h")) != -1){
    
    switch (ca){

    case 'c':
      configFileName = optarg;
      break;
  
    case 'o':
      outFileName = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "c:o:h")) != -1)
  
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  
  //check of the acquired options
  if((configFileName == "") || (outFileName == "")){
    std::cout << "Error: configuration/output file names not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //---------------------------------------------//
  //  load the efficiencies from look up tables  //
  //---------------------------------------------//
  
  //read the configuration file for the efficiency from LookUp Tables
  pt::ptree configtree_effLUT;
  
  //load the effLUT config options                                                                                                                  
  pt::read_info(configFileName, configtree_effLUT);
  
  //create an effLUT object
  effLUT *effLookUpTable = new effLUT(configFileName);
  
  if(effLookUpTable == NULL){
    std::cout << "Error: effLUT object not created." << std::endl;
    exit(EXIT_FAILURE);  
  }
  
  
  //-----------------------------//
  //  retrieve the efficiencies  //
  //-----------------------------//
  
  double KinEff_1[2] = {-1.};
  double KinEff_2[2] = {-1.};
  
  double BDTEff_1[2] = {-1.};
  double BDTEff_2[2] = {-1};
  
  double PIDEff[2] = {-1.};

  double triggerEff[2] = {-1.};
  double recoEff[2] = {-1.};
  double selectionEff[2] = {-1.};
  
  effLookUpTable->GetMeanEfficiencies(KinEff_1, KinEff_2,
                                      BDTEff_1, BDTEff_2,
                                      PIDEff,
                                      triggerEff, recoEff, selectionEff);
  
  std::cout << std::endl;
  std::cout << "Mean kin_eff_1 = " << KinEff_1[0] << " +- " << KinEff_1[1] << std::endl;
  std::cout << "Mean kin_eff_2 = " << KinEff_2[0] << " +- " << KinEff_2[1] << std::endl;
  std::cout << "Mean BDT_eff_1 = " << BDTEff_1[0] << " +- " << BDTEff_1[1] << std::endl;
  std::cout << "Mean BDT_eff_2 = " << BDTEff_2[0] << " +- " << BDTEff_2[1] << std::endl;
  std::cout << "Mean PID_eff = " << PIDEff[0] << " +- " << PIDEff[1] << std::endl;
  std::cout << "Mean trigger_eff = " << triggerEff[0] << " +- " << triggerEff[1] << std::endl;
  std::cout << "Mean reco_eff = " << recoEff[0] << " +- " << recoEff[1] << std::endl;
  std::cout << "Mean selection_eff = " << selectionEff[0] << " +- " << selectionEff[1] << std::endl;
  std::cout << std::endl;
  
  
  //write the result to the output file
  std::ofstream outFile(outFileName, ios::out);
  
  outFile << "#kin_eff_1\n";
  outFile << KinEff_1[0] << " +- " << KinEff_1[1] << "\n";
  outFile << "#kin_eff_2\n";
  outFile << KinEff_2[0] << " +- " << KinEff_2[1] << "\n";
  outFile << "#BDT_eff_1\n";
  outFile << BDTEff_1[0] << " +- " << BDTEff_1[1] << "\n";
  outFile << "#BDT_eff_2\n";
  outFile << BDTEff_2[0] << " +- " << BDTEff_2[1] << "\n";
  outFile << "#PID_eff\n";
  outFile << PIDEff[0] << " +- " << PIDEff[1] << "\n";
  outFile << "#trigger_eff\n";
  outFile << triggerEff[0] << " +- " << triggerEff[1] << "\n";
  outFile << "#reco_eff\n";
  outFile << recoEff[0] << " +- " << recoEff[1] << "\n";
  outFile << "#selection_eff\n";
  outFile << selectionEff[0] << " +- " << selectionEff[1] << "\n";
  outFile.close();
  
  return 0;  
}
