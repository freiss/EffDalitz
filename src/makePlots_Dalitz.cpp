/*!                                  
 *  @file      makePlots_Dalitz.cpp                   
 *  @author    Alessio Piucci                                    
 *  @brief     A macro to make Dalitz plots.                         
 */

#include "../include/Dalitz.h"
#include "../include/Eff.h"
#include "../include/commonLib.h"

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
namespace pt = boost::property_tree;


int main(int argc, char** argv){
  
  //---------------------//
  //  parse job options  //
  //---------------------//
  
  std::string inFileName = "";
  std::string outFileName = "";
  std::string configFileName = "";
  
  extern char* optarg;
  
  int ca;
  
  //parse the input options
  while ((ca = getopt(argc, argv, "i:o:c:h")) != -1){
    switch (ca){

    case 'i':
      inFileName = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
    
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o:c:h")) != -1)
  
  //print the acquired options
  std::cout << "inFileName = " << inFileName << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  
  //check of the acquired options
  if((configFileName == "") || (inFileName == "") || (outFileName == "")){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //configuration Boost tree
  pt::ptree configtree;
  
  //parse the INFO into the property tree
  pt::read_info(configFileName, configtree);
  
  //open the input file
  TFile* inFile = TFile::Open(ParseEnvName(inFileName).c_str(), "READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //open the input tree
  TTree* inTree = (TTree*) inFile->Get((configtree.get<std::string>("options.DecayTree")).c_str());
  
  if(inTree == nullptr){
    std::cout << "Error: input tree does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  std::cout << "inFileName = " << inFileName
            << ", inTree = " << configtree.get<std::string>("options.DecayTree") << std::endl;
  
  //open the output file
  TFile* outFile = new TFile(ParseEnvName(outFileName).c_str(), "RECREATE");
  
  ////////
  ////////
  
  //create a Dalitz object
  Dalitz* DalitzPlot = new Dalitz(configtree, inFileName, outFile);
  
  //create an Eff object
  Eff* EffPlot = new Eff(configtree, inFileName, outFile);
  
  TH2D* h_Dalitz_reco = new TH2D();
  TH2D* h_Dalitz_gen = new TH2D();
  
  //variables used for normalization of histograms
  unsigned int num_gen_events = 0;
  unsigned int num_reco_events = 0;
  
  //compute the Dalitz plot for reconstructed systems
  h_Dalitz_reco = DalitzPlot->MakeDalitzPlot(inTree, false, num_reco_events);
  
  //efficiency computation?
  if((bool) configtree.get<unsigned int>("efficiency.EffOverMC")){
    
    //open the input file with the MC variables
    TFile* inFile_MC = TFile::Open((configtree.get<std::string>("efficiency.inFile")).c_str(),"READ");
    
    if(inFile_MC == nullptr){
      std::cout << "Error: input file with MC variables for the efficiency  does not exist." << std::endl;
      exit(EXIT_FAILURE);  
    }
    
    //open the input tree with the MC variables
    TTree* inTree_MC = (TTree*) inFile_MC->Get((configtree.get<std::string>("efficiency.MCDecayTree")).c_str());
    
    if(inTree_MC == nullptr){
      std::cout << "Error: input tree MC does not exist." << std::endl;
      exit(EXIT_FAILURE);  
    }
    
    //compute the Dalitz plot for generated systems
    h_Dalitz_gen = DalitzPlot->MakeDalitzPlot(inTree_MC, true, num_gen_events);
    
    EffPlot->Compute2DEfficiency(h_Dalitz_gen, h_Dalitz_reco, num_reco_events, num_gen_events);
  }
  
  //write the output log
  EffPlot->WriteLog();
  
  //cleaning of memory
  delete h_Dalitz_reco;
  delete h_Dalitz_gen;
  
  //write and close the output file
  outFile->Write();
  outFile->Close();
  
  return 0;
}
