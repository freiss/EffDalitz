/*!
 *  @file      signal_yield.cpp
 *  @author    Alessio Piucci
 *  @brief     Macro to compute the signal yield corrected by the s-weights and the event-efficiencies.
 *  @return    Returns the value of the corrected signal yield and some histos containing the event efficiencies.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include "TROOT.h"
#include <TFile.h>
#include "TString.h"
#include "TH1D.h"

//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooStats/SPlot.h"

//other libraries
#include "../include/effLUT.cpp"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

int main(int argc, char** argv){
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string outFileName = "";
  std::string configFileName = "";
  TString inFileName = "";
  
  extern char* optarg;

  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "i:c:o:h")) != -1){
    switch (ca){

    case 'i':
      inFileName = optarg;
      break;
        
    case 'o':
      outFileName = optarg;
      break;
    
    case 'c':
      configFileName = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;

    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)
  
  std::cout << "inFileName = " << inFileName << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  
  //check of the acquired options
  if((inFileName == "") || (outFileName == "") || (configFileName == "")){
    std::cout << "Error: configuration or input/output file names not correctly set." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  
  //-------------------------------//
  //  read the configuration file  //
  //-------------------------------//
  
  //create empty property tree object
  pt::ptree configtree;
  
  //variable to switch on/off the dummy efficiencies
  bool dummy_eff;
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  //read the configuration file for the efficiency from LookUp Tables
  pt::ptree configtree_effLUT;
  
  std::string effLUT_configFileName = configtree.get<string>("effLUT.config_file");
  
  std::cout << "effLUT configFileName = " << effLUT_configFileName << std::endl;
  
  //load the effLUT config options                                                                                                                  
  pt::read_info(effLUT_configFileName, configtree_effLUT);
  
  //set dummy efficiency status
  dummy_eff = (bool) configtree.get<unsigned int>("effLUT.dummy_eff.status");
  
  //---------------------------------------------//
  //  load the efficiencies from look up tables  //
  //---------------------------------------------//
  
  effLUT *effLookUpTable = NULL;
  
  //some variables used in case of dummy efficiencies
  double kineff_dummy = 1.;
  double BDTeff_dummy = 1.;
  double PIDeff_dummy = 1.;
  
  double totaleff_dummy = 1.;
  double totaleff_err_dummy = 0.;
  
  //dummy efficiency?
  if(!dummy_eff){
    
    //create an effLUT object
    effLookUpTable = new effLUT(effLUT_configFileName);
    
    if(effLookUpTable == NULL){
      std::cout << "Error: effLUT object not created." << std::endl;
      exit(EXIT_FAILURE);
    }
  }
  else
  {
    //load dummy efficiencies
    kineff_dummy = configtree.get<double>("effLUT.dummy_eff.dummy_kin");
    BDTeff_dummy = configtree.get<double>("effLUT.dummy_eff.dummy_BDT");
    PIDeff_dummy = configtree.get<double>("effLUT.dummy_eff.dummy_PID");
  
    totaleff_dummy = kineff_dummy*BDTeff_dummy*PIDeff_dummy;
    totaleff_err_dummy = 0.;
  }
  
  
  //----------------------------------//
  //  read input data with s-weights  //
  //----------------------------------//
  
  //open input file
  TFile* inFile = new TFile(inFileName, "READ");
  
  if(inFile == NULL){
    std::cout << "Error: inputfile does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  RooWorkspace* workspace = (RooWorkspace*) inFile->Get((configtree.get<std::string>("data.workspace_name")).c_str());
  
  if(workspace == NULL){
    std::cout << "Error: worskspace does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  RooDataSet* dataset = dynamic_cast<RooDataSet*>(workspace->data((configtree.get<std::string>("data.dataset_name")).c_str()));
  
  if(dataset == NULL){
    std::cout << "Error: dataset does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  unsigned int entries_dataset = dataset->numEntries();
  
  std::cout << "dataset entries = " << entries_dataset << std::endl;
  
  
  //--------------------//
  //  declare variables //
  //--------------------//
  
  //variables used to create TLorentz vectors
  std::string E_name = configtree_effLUT.get<std::string>("variables.E_name");
  std::string px_name = configtree_effLUT.get<std::string>("variables.px_name");
  std::string py_name = configtree_effLUT.get<std::string>("variables.py_name");
  std::string pz_name = configtree_effLUT.get<std::string>("variables.pz_name");
  
  //one point of the n-dimensional RooDataSet space
  const RooArgSet* row;
                             
  //retrieve variable names used to build the Dalitz plot
  std::string minv_1_name = configtree.get<std::string>("data.minv_1_name");
  std::string minv_2_name = configtree.get<std::string>("data.minv_2_name");
  
  RooRealVar* minv_1 = NULL;
  RooRealVar* minv_2 = NULL;
  
  //retrieve variable names used for the kinematic efficiency
  std::string Kin_1_name = configtree_effLUT.get<std::string>("kinEff.Kin_1_name");
  std::string Kin_2_name = configtree_effLUT.get<std::string>("kinEff.Kin_2_name");
  
  RooRealVar* Kin_1 = NULL;
  RooRealVar* Kin_2 = NULL;
  
  //retrieve variable names for the BDT efficiency for two cuts
  std::string part_BDT_name_1 = configtree_effLUT.get<std::string>("BDTEff.part_name_1");
  std::string flightdist_BDT_name_1 = configtree_effLUT.get<std::string>("BDTEff.flightdist_name_1");
  
  RooRealVar* px_BDT_1 = NULL;
  RooRealVar* py_BDT_1 = NULL;
  RooRealVar* flightdist_BDT_1 = NULL;
  
  double pt_BDT_1 = -99999.;
  
  std::string part_BDT_name_2 = configtree_effLUT.get<std::string>("BDTEff.part_name_2");
  std::string flightdist_BDT_name_2 = configtree_effLUT.get<std::string>("BDTEff.flightdist_name_2");
  
  RooRealVar* px_BDT_2 = NULL;
  RooRealVar* py_BDT_2 = NULL;
  RooRealVar* flightdist_BDT_2 = NULL;
  
  double pt_BDT_2 = -99999.;
  
  //retrieve variable names for the PID efficiency
  std::string part_PID_name = configtree_effLUT.get<std::string>("PIDEff.part_name");
  std::string ntracks_PID_name = configtree_effLUT.get<std::string>("PIDEff.ntracks_name");
  
  TLorentzVector qvect_PID;
  
  RooRealVar* E_PID = NULL;
  RooRealVar* px_PID = NULL;
  RooRealVar* py_PID = NULL;
  RooRealVar* pz_PID = NULL;
  RooRealVar* ntracks_PID = NULL;
  
  //signal s-weights
  std::string sweight_name = configtree.get<std::string>("data.weights_name");
  
  RooRealVar* s_weight;
  //double s_weight_error;
  
  //efficiency [0] of the current event, and related error [1]
  double event_eff[2];
  
  //final result, the signal yield weighted by s-weights and event efficiency, and its error
  double signal_yield = 0.;
  double signal_yield_eff_error = 0.;
  
  //single components of the signal yield error
  double signal_yield_eff_kintot_error = 0.;
  double signal_yield_eff_BDTtot_error = 0.;
  double signal_yield_eff_PID_error = 0.;
  
  double signal_yield_eff_reco_error = 0.;
  double signal_yield_eff_trigger_error = 0.;
  double signal_yield_eff_selection_error = 0.;
  
  //histos with efficiencies
  TH1D *h_kin_eff_tot = new TH1D("h_kin_eff_tot", "", 100., 0., 0.02);
  TH1D *h_kin_eff_1 = new TH1D("h_kin_eff_1", "", 100., 0., 1.1);
  TH1D *h_kin_eff_2 = new TH1D("h_kin_eff_2", "", 100., 0., 0.02);
  TH1D *h_BDT_eff_tot = new TH1D("h_BDT_eff_tot", "", 100., 0.8, 1.1);
  TH1D *h_BDT_eff_1 = new TH1D("h_BDT_eff_1", "", 100., 0.8, 1.1);
  TH1D *h_BDT_eff_2 = new TH1D("h_BDT_eff_2", "", 100., 0.8, 1.1);
  TH1D *h_PID_eff = new TH1D("h_PID_eff", "", 100., 0.9, 1.1);
  TH1D *h_event_eff = new TH1D("h_event_eff", "", 100., 0., 0.01);
  TH1D *h_trigger_eff = new TH1D("h_trigger_eff", "", 100., 0., 0.01);
  TH1D *h_reco_eff = new TH1D("h_reco_eff", "", 100., 0., 0.01);
  TH1D *h_selection_eff = new TH1D("h_selection_eff", "", 100., 0., 0.01);
  
  //2D plot with efficiency vs error
  TH2D *h_kin_effErr_tot = new TH2D("h_kin_effErr_tot", "", 40., 0., 0.015, 40., 0., 0.015);
  TH2D *h_kin_effErr_1 = new TH2D("h_kin_effErr_1", "", 40., 0., 0.5, 40., 0., 0.5);
  TH2D *h_kin_effErr_2 = new TH2D("h_kin_effErr_2", "", 40., 0., 0.02, 40., 0., 0.04);
  TH2D *h_BDT_effErr_tot = new TH2D("h_BDT_effErr_tot", "", 40., 0.8, 1.1, 40., 0., 0.02);
  TH2D *h_BDT_effErr_1 = new TH2D("h_BDT_effErr_1", "", 40., 0.8, 1.1, 40., 0., 0.015);
  TH2D *h_BDT_effErr_2 = new TH2D("h_BDT_effErr_2", "", 40., 0.8, 1.1, 40., 0., 0.015);
  TH2D *h_PID_effErr = new TH2D("h_PID_effErr", "", 40., 0.9, 1.1, 40., 0., 0.05);
  TH2D *h_event_effErr = new TH2D("h_event_effErr", "", 40., 0., 0.008, 40., 0., 0.01);
  TH2D *h_trigger_effErr = new TH2D("h_trigger_effErr", "", 40., 0., 0.008, 40., 0., 0.01);
  TH2D *h_reco_effErr = new TH2D("h_reco_effErr", "", 40., 0., 0.008, 40., 0., 0.01);
  TH2D *h_selection_effErr = new TH2D("h_selection_effErr", "", 40., 0., 0.008, 40., 0., 0.01);
  
  //average efficiency over Dalitz plot
  TH2D *h_kin_effDalitz_tot = new TH2D("h_kin_effDalitz_tot", "", 30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_kin_effDalitz_1 = new TH2D("h_kin_effDalitz_1", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_kin_effDalitz_2 = new TH2D("h_kin_effDalitz_2", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_BDT_effDalitz_tot = new TH2D("h_BDT_effDalitz_tot", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_BDT_effDalitz_1 = new TH2D("h_BDT_effDalitz_1", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_BDT_effDalitz_2 = new TH2D("h_BDT_effDalitz_2", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_PID_effDalitz = new TH2D("h_PID_effDalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_event_effDalitz = new TH2D("h_event_effDalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_trigger_effDalitz = new TH2D("h_trigger_effDalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_reco_effDalitz = new TH2D("h_reco_effDalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  TH2D *h_selection_effDalitz = new TH2D("h_selection_effDalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  
  TH2D *h_Dalitz = new TH2D("h_Dalitz", "",  30., 16000000, 27000000, 30., 5000000., 12000000.);
  
  //event efficiency [0] and related error [1]
  double kin_eff_tot[2] = {0.};
  double kin_eff_1[2] = {0.};
  double kin_eff_2[2] = {0.};
  
  double BDT_eff_tot[2] = {0.};
  double BDT_eff_1[2] = {0.};
  double BDT_eff_2[2] = {0.};
  
  double PID_eff[2] = {0.};
  
  double trigger_eff[2] = {0.};
  double reco_eff[2] = {0.};
  double selection_eff[2] = {0.};
  
  unsigned int kin_notvalid = 0;
  unsigned int BDT_notvalid = 0;
  unsigned int PID_notvalid = 0;
  
  unsigned int skipped_events = 0;
  
  bool eff_ok = true;
  
  //----------------------------------------------//
  //  correct data by s-weights and efficiencies  //
  //----------------------------------------------//
  
  //loop over candidates
  for(unsigned int i_entry = 0; i_entry < entries_dataset; ++i_entry){
    
    //print the progress
    if((i_entry % 1000) == 0)
      std::cout << "i_entry = " << i_entry << " / " << entries_dataset << std::endl;
    
    //retrieve the current candidate
    row = dataset->get(i_entry);
    
    //load the invariant mass systems, only if the is required
    if((minv_1_name != "") && (minv_2_name != "")){
      
      minv_1 = (RooRealVar*) row->find(minv_1_name.c_str());
      minv_2 = (RooRealVar*) row->find(minv_2_name.c_str());
      
      if((minv_1 == NULL) || (minv_2 == NULL)){
        std::cout << "Error: invariant mass systems not loaded. Please check the variable names." << std::endl;
        exit(EXIT_FAILURE);  
      }
    }
    
    if(!dummy_eff){
     
      //I only need to load these variables for the efficiencies from the look up tables
    
      //load variables for the current candidate
      Kin_1 = (RooRealVar*) row->find(Kin_1_name.c_str());
      Kin_2 = (RooRealVar*) row->find(Kin_2_name.c_str());
      
      if((Kin_1 == NULL) || (Kin_2 == NULL)){
        std::cout << "Error: kinematic variables not loaded. Please check the variable names." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      px_BDT_1 = (RooRealVar*) row->find((part_BDT_name_1 + px_name).c_str());
      py_BDT_1 = (RooRealVar*) row->find((part_BDT_name_1 + py_name).c_str());
      
      pt_BDT_1 = sqrt(pow(px_BDT_1->getVal(), 2.) + pow(py_BDT_1->getVal(), 2.));
      
      flightdist_BDT_1 = (RooRealVar*) row->find(flightdist_BDT_name_1.c_str());
      
      if((px_BDT_1 == NULL) || (py_BDT_1 == NULL) || (flightdist_BDT_1 == NULL)){
        std::cout << "Error: BDT_1 variables not loaded. Please check the variable names." << std::endl;
        exit(EXIT_FAILURE); 
      }
      
      px_BDT_2 = (RooRealVar*) row->find((part_BDT_name_2 + px_name).c_str());
      py_BDT_2 = (RooRealVar*) row->find((part_BDT_name_2 + py_name).c_str());
      
      pt_BDT_2 = sqrt(pow(px_BDT_2->getVal(), 2.) + pow(py_BDT_2->getVal(), 2.));
      
      flightdist_BDT_2 = (RooRealVar*) row->find(flightdist_BDT_name_2.c_str());
      
      if((px_BDT_2 == NULL) || (py_BDT_2 == NULL) || (flightdist_BDT_2 == NULL)){
        std::cout << "Error: BDT_2 variables not loaded. Please check the variable names." << std::endl;
        exit(EXIT_FAILURE); 
      }
      
      E_PID = (RooRealVar*) row->find((part_PID_name + E_name).c_str());
      px_PID = (RooRealVar*) row->find((part_PID_name + px_name).c_str());
      py_PID = (RooRealVar*) row->find((part_PID_name + py_name).c_str());
      pz_PID = (RooRealVar*) row->find((part_PID_name + pz_name).c_str());
      
      qvect_PID.SetPxPyPzE(px_PID->getVal(), py_PID->getVal(), pz_PID->getVal(), E_PID->getVal());
      
      ntracks_PID = (RooRealVar*) row->find(ntracks_PID_name.c_str());
      
      if((E_PID == NULL) || (px_PID == NULL) || (py_PID == NULL) || (pz_PID == NULL) || (ntracks_PID == NULL)){
        std::cout << "Error: PID variables not loaded. Please check the variable names." << std::endl;
        exit(EXIT_FAILURE);
      }
      
    }  //if(!dummy_eff)
    
    s_weight = (RooRealVar*) row->find(sweight_name.c_str());
    
    if(s_weight == NULL){
      std::cout << "Error: sweight variable not loaded. Please check the variable name." << std::endl;
      exit(EXIT_FAILURE); 
    }
    
    //s_weight_error = 0.;  //DUMMY VALUE!
    
    //-------------------------------//
    //  I retrieve the efficiencies  //
    //-------------------------------//
    
    eff_ok = true;
    
    if(!dummy_eff){
      
      effLookUpTable->GetEventEff(Kin_1->getVal(), Kin_2->getVal(),
                                  pt_BDT_1, flightdist_BDT_1->getVal(), pt_BDT_2, flightdist_BDT_2->getVal(),
                                  qvect_PID.P(), qvect_PID.PseudoRapidity(), ntracks_PID->getVal(),
                                  kin_eff_tot, kin_eff_1, kin_eff_2,
                                  BDT_eff_tot, BDT_eff_1, BDT_eff_2,
                                  PID_eff, event_eff);
      
      //some checks of the computed efficiencies,
      //for some counters
      if(kin_eff_tot[0] <= 0.){
        ++kin_notvalid;
        
        eff_ok = false;
      }
      
      if(BDT_eff_tot[0] <= 0.){
        ++BDT_notvalid;
        
        eff_ok = false;
      }
      
      if(PID_eff[0] <= 0.){
        ++PID_notvalid;
        
        eff_ok = false;
      }
    
      //if something was wrong, skip the candidate
      if(!eff_ok){
        ++skipped_events;
        
        continue;
      }
      
      //retrieve the single kinematic components of the efficiency
      effLookUpTable->GetKinComponents(Kin_1->getVal(), Kin_2->getVal(),
                                       trigger_eff, reco_eff, selection_eff);
      
    }  //if(!dummy_eff)
    else
    {
      //dummy efficiencies
      kin_eff_tot[0] = kineff_dummy;
      BDT_eff_tot[0] = BDTeff_dummy;
      PID_eff[0] = PIDeff_dummy;
      event_eff[0] = totaleff_dummy;
      event_eff[1] = totaleff_err_dummy;
    }  //if(!dummy_eff)
    
    
    //to compute the average efficiencies
    h_kin_eff_tot->Fill(kin_eff_tot[0]);
    h_kin_eff_1->Fill(kin_eff_1[0]);
    h_kin_eff_2->Fill(kin_eff_2[0]);
    
    h_BDT_eff_tot->Fill(BDT_eff_tot[0]);
    h_BDT_eff_1->Fill(BDT_eff_1[0]);
    h_BDT_eff_2->Fill(BDT_eff_2[0]);
    
    h_PID_eff->Fill(PID_eff[0]);
    h_event_eff->Fill(event_eff[0]);
    
    h_trigger_eff->Fill(trigger_eff[0]);
    h_reco_eff->Fill(reco_eff[0]);
    h_selection_eff->Fill(selection_eff[0]);
    
    //2D plot with efficiency vs error
    h_kin_effErr_tot->Fill(kin_eff_tot[0], kin_eff_tot[1]);
    h_kin_effErr_1->Fill(kin_eff_1[0], kin_eff_1[1]);
    h_kin_effErr_2->Fill(kin_eff_2[0], kin_eff_2[1]);
    
    h_BDT_effErr_tot->Fill(BDT_eff_tot[0], BDT_eff_tot[1]);
    h_BDT_effErr_1->Fill(BDT_eff_1[0], BDT_eff_1[1]);
    h_BDT_effErr_2->Fill(BDT_eff_2[0], BDT_eff_2[1]);
    
    h_PID_effErr->Fill(PID_eff[0], PID_eff[1]);
    h_event_effErr->Fill(event_eff[0], event_eff[1]);
    
    h_trigger_effErr->Fill(trigger_eff[0], trigger_eff[1]);
    h_reco_effErr->Fill(reco_eff[0], reco_eff[1]);
    h_selection_effErr->Fill(selection_eff[0], selection_eff[1]);
    
    //average efficiency over Dalitz plot
    if((minv_1_name != "") && (minv_2_name != "")){
      
      h_kin_effDalitz_tot->AddBinContent(h_kin_effDalitz_tot->FindBin(minv_1->getVal(), minv_2->getVal()), kin_eff_tot[0]);
      h_kin_effDalitz_1->AddBinContent(h_kin_effDalitz_1->FindBin(minv_1->getVal(), minv_2->getVal()), kin_eff_1[0]);
      h_kin_effDalitz_2->AddBinContent(h_kin_effDalitz_2->FindBin(minv_1->getVal(), minv_2->getVal()), kin_eff_2[0]);
      
      h_BDT_effDalitz_tot->AddBinContent(h_BDT_effDalitz_tot->FindBin(minv_1->getVal(), minv_2->getVal()), BDT_eff_tot[0]);
      h_BDT_effDalitz_1->AddBinContent(h_BDT_effDalitz_1->FindBin(minv_1->getVal(), minv_2->getVal()), BDT_eff_1[0]);
      h_BDT_effDalitz_2->AddBinContent(h_BDT_effDalitz_2->FindBin(minv_1->getVal(), minv_2->getVal()), BDT_eff_2[0]);
      
      h_PID_effDalitz->AddBinContent(h_PID_effDalitz->FindBin(minv_1->getVal(), minv_2->getVal()), PID_eff[0]);
      h_event_effDalitz->AddBinContent(h_event_effDalitz->FindBin(minv_1->getVal(), minv_2->getVal()), event_eff[0]);
      
      h_trigger_effDalitz->AddBinContent(h_trigger_effDalitz->FindBin(minv_1->getVal(), minv_2->getVal()), trigger_eff[0]);
      h_reco_effDalitz->AddBinContent(h_reco_effDalitz->FindBin(minv_1->getVal(), minv_2->getVal()), reco_eff[0]);
      h_selection_effDalitz->AddBinContent(h_selection_effDalitz->FindBin(minv_1->getVal(), minv_2->getVal()), selection_eff[0]);
      
      //Dalitz plot, used to normalize the efficiency averages
      h_Dalitz->Fill(minv_1->getVal(), minv_2->getVal());
    }
    
    //--------------------------------------------//
    //  finally, I can compute the signal yield!  //
    //--------------------------------------------//
    
    double curr_signal = s_weight->getVal() / event_eff[0];
    
    //check if the something gone wrong, with current weighted signal nan
    if(std::isnan(curr_signal)){
      std::cout << "Warning: curr_signal is nan! s_weight->getVal() = " << s_weight->getVal()
                << ", event_eff = " << event_eff[0] << ", i_entry = " << i_entry << std::endl;
      
      continue;
    }
    
    double curr_yield = s_weight->getVal() / event_eff[0];
        
    //double curr_yield_error = sqrt(pow(s_weight_error/event_eff[0], 2.)
    //                               + pow((s_weight->getVal()/pow(event_eff[0],2.))*event_eff[1], 2.));
    
    //sum of sweight and event efficiency
    signal_yield += curr_yield;
    signal_yield_eff_error += pow((s_weight->getVal() / pow(event_eff[0], 2.)) * event_eff[1], 2.);

    //the same for the individual components of the efficiency
    //if E_tot = E_1 * E_2 * ... * E_n, the error contribution due to E_1 is:
    // | d(s-weight/E_tot) / d(E_1) |^2 * delta(E_1)^2 =
    // = | s-weight / (E_1^2 * (E_2 * ... * E_n)) |^2 * delta(E_1)^2
    //then I could further simplify the denominator, writing:
    // = | s-weight / (E_1 * E_tot) |^2 * delta(E_1)^2
    signal_yield_eff_kintot_error += pow((s_weight->getVal() / (kin_eff_tot[0] * event_eff[0]))
                                         * kin_eff_tot[1], 2.);
    
    signal_yield_eff_BDTtot_error += pow((s_weight->getVal() / (BDT_eff_tot[0] * event_eff[0]))
                                         * BDT_eff_tot[1], 2.);
    
    signal_yield_eff_PID_error += pow((s_weight->getVal() / (PID_eff[0] * event_eff[0]))
                                      * PID_eff[1], 2.);
    
    signal_yield_eff_trigger_error += pow((s_weight->getVal() / (trigger_eff[0] * event_eff[0]))
                                          * trigger_eff[1], 2.);
    
    signal_yield_eff_reco_error += pow((s_weight->getVal() / (reco_eff[0] * event_eff[0]))
                                       * reco_eff[1], 2.);
    
    signal_yield_eff_selection_error += pow((s_weight->getVal() / (selection_eff[0] * event_eff[0]))
                                            * selection_eff[1], 2.);
    
  }  //loop over candidates
  
  //root of the signal_yield_eff_error
  signal_yield_eff_error = sqrt(signal_yield_eff_error);
  
  //compute the final efficiencies and errors
  signal_yield_eff_kintot_error = sqrt(signal_yield_eff_kintot_error);
  signal_yield_eff_BDTtot_error = sqrt(signal_yield_eff_BDTtot_error);
  signal_yield_eff_PID_error = sqrt(signal_yield_eff_PID_error);
  
  //normalize the Dalitz plot with the efficiencies
  h_kin_effDalitz_tot->Divide(h_Dalitz);
  h_kin_effDalitz_1->Divide(h_Dalitz);
  h_kin_effDalitz_2->Divide(h_Dalitz);
  
  h_BDT_effDalitz_tot->Divide(h_Dalitz);
  h_BDT_effDalitz_1->Divide(h_Dalitz);
  h_BDT_effDalitz_2->Divide(h_Dalitz);
  
  h_PID_effDalitz->Divide(h_Dalitz);
  h_event_effDalitz->Divide(h_Dalitz);
  
  h_trigger_effDalitz->Divide(h_Dalitz);
  h_reco_effDalitz->Divide(h_Dalitz);
  h_selection_effDalitz->Divide(h_Dalitz);
  
  //print some statistics
  std::cout << std::endl;
  std::cout << "Yield = " << signal_yield
            << " +- " << sqrt(signal_yield) << " (poiss) (" << (1./sqrt(signal_yield))*100 << " %)"
            << " +- " << signal_yield_eff_error << " (eff) (" << (signal_yield_eff_error/signal_yield)*100 << " %)"
            << "( +-" << (signal_yield_eff_kintot_error/signal_yield)*100 << " % kin,"
            << " +-" << (signal_yield_eff_BDTtot_error/signal_yield)*100 << " % BDT,"
            << " +-" << (signal_yield_eff_PID_error/signal_yield)*100 << " % PID)" <<std::endl;
  std::cout << "---> Please note that the errors of the single efficiency contributions and of the total efficiency are computed in a different way!"<< std::endl;
  std::cout << std::endl;

  std::cout << "kin_eff_tot_mean = " << h_kin_eff_tot->GetMean() << ", stdv = " << h_kin_eff_tot->GetStdDev()
            << " (" << (h_kin_eff_tot->GetStdDev() / h_kin_eff_tot->GetMean())*100 << "%)"
            << ", meanerr = " << h_kin_eff_tot->GetMeanError()
            << " (" << (h_kin_eff_tot->GetMeanError() / h_kin_eff_tot->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> kin_eff_1_mean = " << h_kin_eff_1->GetMean() << ", stdv = " << h_kin_eff_1->GetStdDev()
            << " (" << (h_kin_eff_1->GetStdDev() / h_kin_eff_1->GetMean())*100 << "%)"
            << ", meanerr = " << h_kin_eff_1->GetMeanError()
            << " (" << (h_kin_eff_1->GetMeanError() / h_kin_eff_1->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> kin_eff_2_mean = " << h_kin_eff_2->GetMean() << ", stdv = " << h_kin_eff_2->GetStdDev()
            << " (" << (h_kin_eff_2->GetStdDev() / h_kin_eff_2->GetMean())*100 << "%)" 
            << ", meanerr = " << h_kin_eff_2->GetMeanError()
            << " (" << (h_kin_eff_2->GetMeanError() / h_kin_eff_2->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "BDT_eff_tot_mean = " << h_BDT_eff_tot->GetMean() << ", stdv = " << h_BDT_eff_tot->GetStdDev() 
            << " (" << (h_BDT_eff_tot->GetStdDev() / h_BDT_eff_tot->GetMean())*100 << "%)"
            << ", meanerr = " << h_BDT_eff_tot->GetMeanError()
            << " (" << (h_BDT_eff_tot->GetMeanError() / h_BDT_eff_tot->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_tot_mean = " << h_BDT_eff_1->GetMean() << ", stdv = " << h_BDT_eff_1->GetStdDev()
            << " (" << (h_BDT_eff_1->GetStdDev() / h_BDT_eff_1->GetMean())*100 << "%)"
            << ", meanerr = " << h_BDT_eff_1->GetMeanError()
            << " (" << (h_BDT_eff_1->GetMeanError() / h_BDT_eff_1->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_tot_mean = " << h_BDT_eff_2->GetMean() << ", stdv = " << h_BDT_eff_2->GetStdDev()
            << " (" << (h_BDT_eff_2->GetStdDev() / h_BDT_eff_2->GetMean())*100 << "%)"
            << ", meanerr = " << h_BDT_eff_2->GetMeanError()
            << " (" << (h_BDT_eff_2->GetMeanError() / h_BDT_eff_2->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "PID_eff_mean = " << h_PID_eff->GetMean() << ", stdv = " << h_PID_eff->GetStdDev() 
            << " (" << (h_PID_eff->GetStdDev() / h_PID_eff->GetMean())*100 << "%)"
            << ", meanerr = " << h_PID_eff->GetMeanError()
            << " (" << (h_PID_eff->GetMeanError() / h_PID_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  std::cout << "event_eff_mean = " << h_event_eff->GetMean() << ", stdv = " << h_event_eff->GetStdDev() 
            << " (" << (h_event_eff->GetStdDev() / h_event_eff->GetMean())*100 << "%)"
            << ", meanerr = " << h_event_eff->GetMeanError()
            << " (" << (h_event_eff->GetMeanError() / h_event_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "kin_notvalid = " << kin_notvalid << ", "
            << (kin_notvalid / (double) entries_dataset)*100 << "%" << std::endl;
  std::cout << "BDT_notvalid = " << BDT_notvalid << ", "
            << (BDT_notvalid / (double) entries_dataset)*100 << "%" << std::endl;
  std::cout << "PID_notvalid = " << PID_notvalid << ", "
            << (PID_notvalid / (double) entries_dataset)*100 << "%" << std::endl;
  std::cout << std::endl;
  std::cout << "skipped_events = " << skipped_events << ", "
            << (skipped_events / (double) entries_dataset)*100 << "%" << std::endl;
  std::cout << std::endl;

  //if the not-valid cases are more then 1% of entries, print a warning
  if((kin_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid kinematic cases! Please have a look!" << std::endl;
  
  if((BDT_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid BDT cases! Please have a look!" << std::endl;
  
  if((PID_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid PID cases! Please have a look!" << std::endl;
  
  if((skipped_events / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many skipped events! Please have a look!" << std::endl;
  std::cout << std::endl;
  
  
  //save the results in a output INFO file
  pt::ptree log_out;
  
  log_out.put<double>("Signal_yield.value", signal_yield);
  log_out.put<double>("Signal_yield.Error.Poissonian.value", sqrt(signal_yield));
  log_out.put<double>("Signal_yield.Error.Efficiency.value", signal_yield_eff_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyKin.value", signal_yield_eff_kintot_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyBDT.value", signal_yield_eff_BDTtot_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyPID.value", signal_yield_eff_PID_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyTrigger.value", signal_yield_eff_trigger_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyReconstruction.value", signal_yield_eff_reco_error);
  log_out.put<double>("Signal_yield.Error.EfficiencySelectionKin.value", signal_yield_eff_selection_error);
  
  write_info(outFileName + "_log.info", log_out);
  
  //write the histos to the output root file
  TFile* outFile_histos = new TFile((outFileName + "_histos.root").c_str(), "RECREATE");
  
  outFile_histos->cd();
  
  h_kin_eff_tot->Write("h_kin_eff_tot");
  h_kin_eff_1->Write("h_kin_eff_1");
  h_kin_eff_2->Write("h_kin_eff_2");
  h_BDT_eff_tot->Write("h_BDT_eff_tot");
  h_BDT_eff_1->Write("h_BDT_eff_1");
  h_BDT_eff_2->Write("h_BDT_eff_2");
  h_PID_eff->Write("h_PID_eff");
  h_event_eff->Write("h_event_eff");
  h_trigger_eff->Write("h_trigger_eff");
  h_reco_eff->Write("h_reco_eff");
  h_selection_eff->Write("h_selection_eff");
  
  h_kin_effErr_tot->Write("h_kin_effErr_tot");
  h_kin_effErr_1->Write("h_kin_effErr_1");
  h_kin_effErr_2->Write("h_kin_effErr_2");
  h_BDT_effErr_tot->Write("h_BDT_effErr_tot");
  h_BDT_effErr_1->Write("h_BDT_effErr_1");
  h_BDT_effErr_2->Write("h_BDT_effErr_2");
  h_PID_effErr->Write("h_PID_effErr");
  h_event_effErr->Write("h_event_effErr");
  h_trigger_effErr->Write("h_trigger_effErr");
  h_reco_effErr->Write("h_reco_effErr");
  h_selection_effErr->Write("h_selection_effErr");
  
  h_kin_effDalitz_tot->Write("h_kin_effDalitz_tot");
  h_kin_effDalitz_1->Write("h_kin_effDalitz_1");
  h_kin_effDalitz_2->Write("h_kin_effDalitz_2");
  h_BDT_effDalitz_tot->Write("h_BDT_effDalitz_tot");
  h_BDT_effDalitz_1->Write("h_BDT_effDalitz_1");
  h_BDT_effDalitz_2->Write("h_BDT_effDalitz_2");
  h_PID_effDalitz->Write("h_PID_effDalitz");
  h_event_effDalitz->Write("h_event_effDalitz");
  h_trigger_effDalitz->Write("h_trigger_effDalitz");
  h_reco_effDalitz->Write("h_reco_effDalitz");
  h_selection_effDalitz->Write("h_selection_effDalitz");
  
  h_Dalitz->Write("h_Dalitz");
  
  
  outFile_histos->Write();
  outFile_histos->Close();
  
  return 0;
}
